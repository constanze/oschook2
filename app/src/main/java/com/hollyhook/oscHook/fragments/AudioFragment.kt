package com.hollyhook.oscHook.fragments

import android.os.*
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hollyhook.oscHook.*
import com.jjoe64.graphview.DefaultLabelFormatter
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.PointsGraphSeries
import java.util.*

/**
 * A [PlotFragment] subclass for the audio
 */
class AudioFragment : PlotFragment(
    OscHookService.MSG_REGISTER_AUDIO_CLIENT,
    OscHookService.MSG_UNREGISTER_AUDIO_CLIENT
) {
    override var TAG = "oscHook.AudioFragment"

    private val splDataSeries = PointsGraphSeries<DataPoint>() // spl data
    private val frequencyDataSeries = PointsGraphSeries<DataPoint>() // secondary axis

    // initializer block
    init {
        setIncomingHandler(IncomingHandler())
    }

    // called by the service
    inner class IncomingHandler : Handler() {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                OscHookService.MSG_AUDIO -> {
                    if (msg.peekData() == null)
                        plotData() // just scroll a bit
                    else {
                        try {
                            // re-using the preferences key here
                            val key1 = getString(R.string.audio_osc_spl_key)
                            val key2 = getString(R.string.audio_osc_frequency_key)
                            val spl: Float = msg.data.getFloat(key1)
                            val frequency: Float = msg.data.getFloat(key2)

                            plotData(spl.toDouble(), frequency.toDouble())
                        } catch (ex: IllegalStateException) {
                            // the fragment might be dead already
                        }
                    }
                }
                else -> super.handleMessage(msg)
            }
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.v(TAG, "onCreateView")
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_audio, container, false)
    }


    override fun setupGraphView(speedSetting: Int) {
        super.setupGraphView(speedSetting)
        splDataSeries.isInLegend = true
        splDataSeries.title =getString(R.string.legend_raw)
        splDataSeries.color = getThemeColor(context, "colorPrimary")
        splDataSeries.strokeWidth = if (speedSetting == 0) 3f else 1f
        splDataSeries.size = dip2pixel(PLOT_SHAPE_SIZE).toFloat()

        graphView?.addSeries(splDataSeries)

        // second set uses second scale
        frequencyDataSeries.isInLegend = true
        frequencyDataSeries.title = getString(R.string.legend_raw)
        frequencyDataSeries.color = COLOR_PLOT3.toInt()
        frequencyDataSeries.strokeWidth = splDataSeries.strokeWidth
        frequencyDataSeries.size = dip2pixel(PLOT_SHAPE_SIZE).toFloat()

        graphView?.secondScale?.addSeries(frequencyDataSeries)
        // the y bounds are always manual for second scale
        graphView?.secondScale?.setMinY(0.0)
        graphView?.secondScale?.setMaxY(2.0) // kHz
        graphView?.secondScale?.labelFormatter = object: DefaultLabelFormatter() {
            override fun formatLabel(value: Double, isValueX: Boolean):String {
                return if (isValueX) {
                    // show normal x values
                    super.formatLabel(value, isValueX);
                } else {
                    // show currency for y values
                    super.formatLabel(value, isValueX) + " kHz";
                }
            }
        }

        graphView?.gridLabelRenderer?.verticalLabelsSecondScaleColor = COLOR_PLOT3.toInt()
        //graphView?.gridLabelRenderer?.padding = 6+0

        // set time axis label formatter
        graphView?.title = resources.getString(R.string.audio_title)
    }


    fun plotData(spl: Double, frequency: Double) {
        val stepCounter = Date(System.currentTimeMillis())

        // have an invisible data point to keep the chart scrolling
        // NOT THREAD SAFE
        activity?.runOnUiThread {
            invisibleSeries.appendData(DataPoint(stepCounter, 0.0), true, DATA_LEN)
            splDataSeries.appendData(DataPoint(stepCounter, spl), true, DATA_LEN)
            // scale is kHz
            frequencyDataSeries.appendData(DataPoint(stepCounter, frequency/1000), true, DATA_LEN)
        }
    }
}