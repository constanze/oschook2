package com.hollyhook.oscHook.fragments

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.graphics.Color
import android.os.*
import android.util.Log
import android.util.TypedValue
import android.view.Surface
import android.view.View
import android.view.WindowManager
import androidx.activity.addCallback
import androidx.annotation.CallSuper
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.hollyhook.oscHook.*
import com.jjoe64.graphview.GraphView
import com.jjoe64.graphview.LegendRenderer
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.PointsGraphSeries
import java.text.DateFormat
import java.util.*

/**
 * base class for the fragments showing the graphview
 */

open class PlotFragment(val serviceRegisterId: Int, 
                        private val serviceUnregisterId: Int,
                        var secondPlot: Boolean = false) : Fragment() {
    open var TAG = "oscHook.PlotFragment"

    var messageToService: Messenger? = null
    var messageFromService: Messenger? = null

    val invisibleSeries = PointsGraphSeries<DataPoint>() // invisible

    var graphView: GraphView? = null
    var graphView2: GraphView? = null

    // Use this instance of the interface to get members of the parent
    lateinit var parent: MainActivity

    val LANDSCAPE = 0
    val PORTRAIT = 0

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            parent = context as MainActivity
        } catch (e: ClassCastException) {
            Log.e(TAG, "invalid parent class")
        }
    }

    fun setIncomingHandler(h: Handler) {
        messageFromService = Messenger(h)
    }

    private val mConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            Log.d(TAG, "onServiceConnected")
            messageToService = Messenger(service)
            try {
                // register to service as a client to myService
                val msg: Message = Message.obtain(null, serviceRegisterId)
                msg.replyTo = messageFromService
                messageToService!!.send(msg)
            } catch (e: RemoteException) {
                // In this case the service has crashed before we could even do anything with it
            }
        }

        override fun onServiceDisconnected(className: ComponentName) {
            Log.d(TAG, "onServiceDisconnected")
            // This is called when the connection with the service has been unexpectedly disconnected - process crashed.
            messageToService = null
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d(TAG, "onCreate")
        super.onCreate(savedInstanceState)
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            findNavController().popBackStack()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.d(TAG, "onViewCreated")
        super.onViewCreated(view, savedInstanceState)

        // show my menu entry
        activity?.invalidateOptionsMenu()
        // setup plot
        graphView = view.findViewById(R.id.graph_view)
        if (secondPlot)
            graphView2 = view.findViewById(R.id.graph_view2)

        val speedSetting = parent.sharedPref?.getInt(getString(R.string.update_rate_key), 1)

        setupGraphView(speedSetting?:1)
    }

    override fun onResume () {
        Log.d(TAG, "onResume")
        super.onResume()
        doBindService()
    }

    override fun onPause() {
        Log.d(TAG, "onPause")
        super.onPause()
        try {
            doUnbindService()
        } catch (t: Throwable) {
            Log.e(TAG, "Failed to unbind from the service", t)
        }
    }

    override fun onDestroyView() {
        Log.v(TAG, "onDestroyView")

        // hide my menu entry
        activity?.invalidateOptionsMenu()

        super.onDestroyView()
    }

    /*
     * bind to the service. This starts it in case it is not yet running
     */
    private fun doBindService() {
        parent.bindService(Intent(parent, OscHookService::class.java), mConnection, Context.BIND_AUTO_CREATE)
    }

    private fun doUnbindService() {
        Log.d(TAG, "unbinding")
        // If we have received the service, and hence registered with it,
        // then now is the time to unregister.
        if (messageToService != null) {
            try {
                val msg: Message = Message.obtain(null, serviceUnregisterId)
                messageToService!!.send(msg)
            } catch (e: RemoteException) {
                // There is nothing special we need to do if the service has crashed.
            }
        }
        // Detach our existing connection.
        parent.unbindService(mConnection)
    }

    protected fun sendMessageToService(msgId: Int) {
        Log.d(TAG, "sendMessageToService $msgId")
        if (messageToService != null) {
            try {
                val msg: Message = Message.obtain(null, msgId)
                messageToService!!.send(msg)
            } catch (e: RemoteException) {
                // There is nothing special we need to do if the service has crashed.
            }
        }
    }

    fun getThemeColor(context: Context?, name: String): Int {
        data class ColorMapping( val default: Int, val attr: Int, val name: String)

        val colorMappings: Array<ColorMapping> = arrayOf(
            ColorMapping(0x1c1b20,
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    android.R.attr.colorPrimaryDark
                else
                    0,
                "colorPrimaryDark"),
            ColorMapping(0x383f51,
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    android.R.attr.colorPrimary
                else
                    0,
                "colorPrimary"),
            ColorMapping(0x58a4b1,
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    android.R.attr.colorAccent
                else
                    0,
                "colorAccent")
        )

        val myColMapping = colorMappings.find { it.name == name } ?: return Color.RED
        if (context == null) return myColMapping.default

        val colorAttr: Int = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            myColMapping.attr
        } else {
            //Get colorPrimary defined for AppCompat
            context.resources.getIdentifier(name, "attr", context.packageName)
        }
        val outValue = TypedValue()
        context.theme.resolveAttribute(colorAttr, outValue, true)
        return outValue.data
    }


    /*
    * speed setting is for adjusting the scale for fast / slow update rate
    * a value of 0 means slow
    */
    @CallSuper
    open fun setupGraphView(speedSetting: Int) {
        // this is only for nice horizontal scrolling
        invisibleSeries.color = Color.TRANSPARENT
        invisibleSeries.isInLegend = false
        invisibleSeries.appendData(DataPoint(Date(System.currentTimeMillis()), 0.0), true, DATA_LEN)

        setupGraphView(graphView!!, speedSetting)
        if (secondPlot)
            setupGraphView(graphView2!!, speedSetting)

    }

    private fun setupGraphView(gv: GraphView, speedSetting: Int) {
        // setup graph view
        gv.addSeries(invisibleSeries) // This must be done first, to prevent 1.1.1970 label on the x axis

        // as we use dates as labels, the human rounding to nice readable numbers
        // is not necessary, crashes anyway
        gv.gridLabelRenderer?.setHumanRounding(false, true)
        gv.gridLabelRenderer?.padding = dip2pixel(35f)

        // 0 means slow, ca. 200 ms per measurement
        val delta = if (speedSetting == 0) 50000.0 else 25000.0

        // activate horizontal scrolling, unit is ms
        gv.viewport?.isXAxisBoundsManual =
            true // this allows automated scrolling to the end
        val now = System.currentTimeMillis().toDouble()
        gv.viewport?.setMinX(now)
        gv.viewport?.setMaxX(now + delta) // milliseconds, one bar is always visible

        gv.legendRenderer?.isVisible = false
        gv.legendRenderer?.align = LegendRenderer.LegendAlign.TOP
        gv.legendRenderer?.backgroundColor = Color.argb(180, 200, 200, 200)
        gv.legendRenderer?.textSize = dip2pixel(LEGEND_TEXT_SIZE).toFloat()

        // set time axis label formatter
        val df = DateFormat.getTimeInstance(DateFormat.SHORT)
        gv.gridLabelRenderer?.labelFormatter = DateAsXAxisLabelFormatter(activity, df)
        gv.gridLabelRenderer?.numHorizontalLabels =
            3 // only 2 because who cares, the 3rd is not working
    }

    // just plot the invisible series, in order to scroll a little
    fun plotData() {
        val stepCounter = Date(System.currentTimeMillis())
        // NOT THREAD SAFE
        activity?.runOnUiThread {
            // have an invisible data point to keep the chart scrolling
            invisibleSeries.appendData(DataPoint(stepCounter, 0.0), true, DATA_LEN)
        }
    }


    fun dip2pixel(dipValue: Float): Int {
        if (context == null) return 0 // already killed
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            dipValue,
            resources.displayMetrics
        ).toInt()
    }

    fun getDisplayRotation(): Int{
        if (context == null) return PORTRAIT
        val display = (requireContext().getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay
        return when (display.rotation){
            Surface.ROTATION_90 -> LANDSCAPE
            Surface.ROTATION_270 -> LANDSCAPE
            Surface.ROTATION_180 -> PORTRAIT
            Surface.ROTATION_0 -> PORTRAIT
            else ->
                PORTRAIT
        }
    }

}