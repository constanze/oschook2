package com.hollyhook.oscHook.fragments

import android.os.*
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat.getColor
import com.hollyhook.oscHook.*

import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.PointsGraphSeries
import java.util.*

/**
 * A [PlotFragment] subclass for the Beacons
 */
class BeaconFragment : PlotFragment(
    OscHookService.MSG_REGISTER_BEACON_CLIENT,
    OscHookService.MSG_UNREGISTER_BEACON_CLIENT
) {

    override var TAG = "oscHook.BeaconFragment"
    private lateinit var beaconOscKeys: Array<String>
    private var sensorDataSeries: Array<PointsGraphSeries<DataPoint>>
            = Array(MAX_BEACONS) { PointsGraphSeries<DataPoint>()}

    // initializer block
    init {
        setIncomingHandler(IncomingHandler())
    }

    // called by the service
    inner class IncomingHandler : Handler() {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                OscHookService.MSG_BEACON -> {
                    if (msg.peekData() == null)
                        plotData() // just scroll a bit
                    else {
                        // re-using the preferences key here
                        val beacon = FloatArray(beaconOscKeys.size)
                        var legendChange = false
                        for (i in beaconOscKeys.indices) {
                            val key = beaconOscKeys[i]
                            try {
                                beacon[i] = msg.data.getFloat(key)
                                val address = msg.data.getString("$key.name")
                                if (address != null) {
                                    if (updateLegend(i, address))
                                        legendChange = true
                                }
                            } catch (ex: IllegalStateException) {
                                // the fragment might be dead already
                            }
                        }
                        // Log.v(TAG, "Beacon $beacon[0], $beacon[1], $beacon[2]")

                        if (legendChange) {
                            val isInvisible: Boolean = sensorDataSeries.all { it.title.isBlank() }
                            graphView?.legendRenderer?.isVisible = !isInvisible
                            // statement has the side-effect that the legend size gets recalculated
                            graphView?.legendRenderer?.textSize =
                                dip2pixel(LEGEND_TEXT_SIZE).toFloat()

                        }
                        plotData(beacon)
                    }
                }
                else -> super.handleMessage(msg)
            }
        }
    }

    // returns true if there was an update
    private fun updateLegend(i: Int, string: String): Boolean {
        if (sensorDataSeries[i].title != string) {
            sensorDataSeries[i].title = string
            sensorDataSeries[i].isInLegend = string.isNotBlank()
            return true
        }
        return false
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d(TAG, "onCreateView")

        beaconOscKeys = arrayOf(getString(R.string.beacon_1_osc_key),
            getString(R.string.beacon_2_osc_key),
            getString(R.string.beacon_3_osc_key),
            getString(R.string.beacon_4_osc_key),
        )
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_beacon, container, false)
    }


     // called from super onViewCreated
     override fun setupGraphView(speedSetting: Int) {
         super.setupGraphView(speedSetting)
         graphView?.legendRenderer?.isVisible = false

         for (i:Int in sensorDataSeries.indices) {
             sensorDataSeries[i].title = ""
             sensorDataSeries[i].isInLegend = false
         }

         sensorDataSeries[0].color = getThemeColor(context, "colorAccent")
         sensorDataSeries[1].color = getThemeColor(context, "colorPrimary")
         sensorDataSeries[2].color = COLOR_PLOT3.toInt() // orange
         sensorDataSeries[3].color = getColor(requireContext(), R.color.colorBright)

         for (i:Int in sensorDataSeries.indices) {
             sensorDataSeries[i].strokeWidth = 5f
             sensorDataSeries[i].size = dip2pixel(PLOT_SHAPE_SIZE).toFloat()
         }

         sensorDataSeries[3].strokeWidth = 7f // more width due to weak contrast
         sensorDataSeries[3].shape = PointsGraphSeries.Shape.TRIANGLE

         for (sds in sensorDataSeries) {
             graphView?.addSeries(sds)
         }

        // set time axis label formatter
        graphView?.title = resources.getString(R.string.beacon_title)
    }


    fun plotData(values: FloatArray) {
        val stepCounter = Date(System.currentTimeMillis())
        // have an invisible data point to keep the chart scrolling
        // NOT THREAD SAFE
        activity?.runOnUiThread {
            invisibleSeries.appendData(DataPoint(stepCounter, 0.0), true, DATA_LEN)
            for (i in values.indices) {
                if (!values[i].isNaN())
                    sensorDataSeries[i].appendData(
                        DataPoint(stepCounter, values[i].toDouble()),
                        true,
                        DATA_LEN
                    )
            }
        }
    }


}