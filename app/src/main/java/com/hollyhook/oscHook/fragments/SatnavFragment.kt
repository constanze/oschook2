
package com.hollyhook.oscHook.fragments


import android.content.SharedPreferences
import android.os.*
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.google.android.material.snackbar.Snackbar
import com.hollyhook.oscHook.*
import com.hollyhook.oscHook.OscHookService.Companion.MSG_SATNAV_RESET_ORIGIN
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.PointsGraphSeries
import java.util.*

/**
 * A [PlotFragment] subclass for Satnav
 */
class SatnavFragment : PlotFragment (
    OscHookService.MSG_REGISTER_SATNAV_CLIENT,
    OscHookService.MSG_UNREGISTER_SATNAV_CLIENT,
    true
), SharedPreferences.OnSharedPreferenceChangeListener,
    View.OnClickListener{

    override var TAG = "oscHook.SatnavFragment"
    private lateinit var satnavOscKeys: Array<String>
    private lateinit var longitudeDataSeries: PointsGraphSeries<DataPoint>
    private lateinit var latitudeDataSeries: PointsGraphSeries<DataPoint>

    private lateinit var resetPositionButton: Button

    // initializer block
    init {
        setIncomingHandler(IncomingHandler())
    }

    // called by the service
    inner class IncomingHandler : Handler() {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                OscHookService.MSG_SATNAV -> {
                    if (msg.peekData() == null)
                        plotData() // just scroll a bit
                    else {
                        // re-using the preferences key here
                        val satnav = FloatArray(2)
                        for (i in satnavOscKeys.indices) {
                            val key = satnavOscKeys[i]
                            try {
                                satnav[i] = msg.data.getFloat(key)
                            } catch (ex: IllegalStateException) {
                                // the fragment might be dead already
                            }
                        }
                        // Log.v(TAG, "Satnav $satnav[0], $satnav[1], $satnav[2]")
                        plotData(satnav[0], satnav[1])
                    }
                }
                else -> super.handleMessage(msg)
            }
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d(TAG, "onCreateView")

        parent.sharedPref?.registerOnSharedPreferenceChangeListener(this)

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_satnav, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        resetPositionButton = view.findViewById(R.id.satnav_reset_button)
        resetPositionButton.setOnClickListener(this)
        super.onViewCreated(view, savedInstanceState)
    }


    // called from super onViewCreated
    override fun setupGraphView(speedSetting: Int) {
        super.setupGraphView(speedSetting)
        graphView?.legendRenderer?.isVisible = true
        graphView2?.legendRenderer?.isVisible = true

        longitudeDataSeries = PointsGraphSeries<DataPoint>()
        latitudeDataSeries = PointsGraphSeries<DataPoint>()

        longitudeDataSeries.color = getThemeColor(context, "colorAccent")
        latitudeDataSeries.color = getThemeColor(context, "colorPrimary")

        for (sds in setOf(longitudeDataSeries, latitudeDataSeries)) {
            sds.isInLegend = true
            sds.strokeWidth = 5f
            sds.size = dip2pixel(PLOT_SHAPE_SIZE).toFloat()
        }
        graphView?.addSeries(longitudeDataSeries)
        graphView2?.addSeries(latitudeDataSeries)

        if (parent.sharedPref != null)
            updateTitle(parent.sharedPref!!)
    }


    fun plotData(lo: Float, la: Float) {
        val stepCounter = Date(System.currentTimeMillis())

        // have an invisible data point to keep the chart scrolling
        // NOT THREAD SAFE, therefore on UI thread
        activity?.runOnUiThread {
            invisibleSeries.appendData(DataPoint(stepCounter, 0.0), true, DATA_LEN)
            longitudeDataSeries.appendData(DataPoint(stepCounter, lo.toDouble()), true, DATA_LEN)
            latitudeDataSeries.appendData(DataPoint(stepCounter, la.toDouble()), true, DATA_LEN)
        }
    }

    override fun onSharedPreferenceChanged(pref: SharedPreferences?, key: String?) {
        Log.v(TAG, "onSharedPreferenceChanged")
        try { // getString fails if no context
            updateTitle(pref)
        } catch (e: IllegalStateException) {}
    }

    // update headlines according to the mode
    private fun updateTitle(pref: SharedPreferences?) {

        val spinnerSelection = pref?.getInt(
            getString(R.string.satnav_spinner_key),
            SATNAV_SPINNER_DEFAULT)?:SATNAV_SPINNER_DEFAULT
        val useOffset: Boolean = SATNAV_SPINNER_DEFAULT == spinnerSelection

        resetPositionButton.isEnabled = useOffset
        resetPositionButton.isClickable = useOffset

        if (useOffset) {
            satnavOscKeys = arrayOf(
                getString(R.string.satnav_distance_osc_key),
                getString(R.string.satnav_bearing_osc_key)
            )
            graphView?.title = getString(R.string.satnav_title_distance)
            graphView2?.title = getString(R.string.satnav_title_bearing)
            longitudeDataSeries.title = getString(R.string.distance)
            latitudeDataSeries.title = getString(R.string.bearing)
        } else {
            satnavOscKeys = arrayOf(
                getString(R.string.satnav_longitude_osc_key),
                getString(R.string.satnav_latitude_osc_key)
            )
            graphView?.title = getString(R.string.satnav_title)
            if (getDisplayRotation() == LANDSCAPE)
                graphView2?.title = " " // to have same height
            longitudeDataSeries.title = getString(R.string.longitude)

            latitudeDataSeries.title = getString(R.string.latitude)
        }
        // statement has the side-effect that the legend size gets recalculated
        graphView?.legendRenderer?.textSize =
            dip2pixel(LEGEND_TEXT_SIZE).toFloat()
    }


    override fun onClick(v: View?) {
        Log.i(TAG, "onResetButtonClicked")
        // plot vertical line to show that the data differs?
        if (v != null) {
            sendMessageToService(MSG_SATNAV_RESET_ORIGIN)
            Snackbar.make(v, getText(R.string.satnav_reset_confirm), Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
    }
}