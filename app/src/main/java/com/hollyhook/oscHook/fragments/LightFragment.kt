package com.hollyhook.oscHook.fragments

import android.os.*
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hollyhook.oscHook.DATA_LEN
import com.hollyhook.oscHook.OscHookService
import com.hollyhook.oscHook.PLOT_SHAPE_SIZE
import com.hollyhook.oscHook.R
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.PointsGraphSeries
import java.util.*

/**
 * A [PlotFragment] subclass for the light
 */
class LightFragment : PlotFragment(
    OscHookService.MSG_REGISTER_LIGHT_CLIENT,
    OscHookService.MSG_UNREGISTER_LIGHT_CLIENT
) {

    override var TAG = "oscHook.LightFragment"

    private val sensorDataSeries = PointsGraphSeries<DataPoint>() // sensor data

    // initializer block
    init {
        setIncomingHandler(IncomingHandler())
    }

    // called by the service
    inner class IncomingHandler : Handler() {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                OscHookService.MSG_LIGHT -> {
                    try {
                        // re-using the preferences key here
                        val key = getString(R.string.light_osc_key)
                        if (msg.peekData() == null)
                            plotData() // just scroll a bit
                        else {
                            val light: Float = msg.data.getFloat(key)
                            val lightRaw: Float = msg.data.getFloat(key + "_raw")
                            plotData(light)
                            Log.v(TAG, "Light $light (raw $lightRaw)")
                        }
                    } catch (ex: IllegalStateException) {
                        Log.w(TAG, ex.message?: ex.toString())
                        // the fragment might be dead already
                    }
                }
                else -> super.handleMessage(msg)
            }
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.v(TAG, "onCreateView")
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_light, container, false)
    }


     override fun setupGraphView(speedSetting: Int) {
        super.setupGraphView(speedSetting)
        sensorDataSeries.isInLegend = true
        sensorDataSeries.title =getString(R.string.legend_raw)
        sensorDataSeries.color = getThemeColor(context, "colorAccent")
        sensorDataSeries.strokeWidth = if (speedSetting == 0) 3f else 1f
        sensorDataSeries.size = dip2pixel(PLOT_SHAPE_SIZE+1).toFloat()

        graphView?.addSeries(sensorDataSeries)

        // set time axis label formatter
        graphView?.title = resources.getString(R.string.light_title)
    }


    fun plotData(lux: Float) {
        val stepCounter = Date(System.currentTimeMillis())
        // NOT THREAD SAFE
        activity?.runOnUiThread {
            // have an invisible data point to keep the chart scrolling
            invisibleSeries.appendData(DataPoint(stepCounter, 0.0), true, DATA_LEN)
            sensorDataSeries.appendData(DataPoint(stepCounter, lux.toDouble()), true, DATA_LEN)
        }
    }
}