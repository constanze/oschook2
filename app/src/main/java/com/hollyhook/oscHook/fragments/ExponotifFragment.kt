package com.hollyhook.oscHook.fragments

import android.os.*
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat.getColor
import com.hollyhook.oscHook.*

import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.PointsGraphSeries
import java.util.*

/**
 * A [PlotFragment] subclass for the Exponotifs
 */
class ExponotifFragment : PlotFragment(
    OscHookService.MSG_REGISTER_EXPONOTIF_CLIENT,
    OscHookService.MSG_UNREGISTER_EXPONOTIF_CLIENT
)
{
    override var TAG = "oscHook.ExponotifFragment"
    private lateinit var exponotifOscKeys: Array<String>
    private var sensorDataSeries: Array<PointsGraphSeries<DataPoint>>
            = Array(MAX_EXPONOTIFS) { PointsGraphSeries<DataPoint>()}

    // initializer block
    init {
        setIncomingHandler(IncomingHandler())
    }

    // called by the service
    inner class IncomingHandler : Handler() {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                OscHookService.MSG_EXPONOTIF -> {
                    if (msg.peekData() == null)
                        plotData() // just scroll a bit
                    else {
                        // re-using the preferences key here
                        val exponotif = FloatArray(exponotifOscKeys.size)
                        var legendChange = false
                        for (i in exponotifOscKeys.indices) {
                            val key = exponotifOscKeys[i]
                            try {
                                exponotif[i] = msg.data.getFloat(key)
                                val address = msg.data.getString("$key.name")
                                if (address != null) {
                                    if (updateLegend(i, address))
                                        legendChange = true
                                }
                            } catch (ex: IllegalStateException) {
                                // the fragment might be dead already
                            }
                        }
                        // Log.v(TAG, "Exponotif $exponotif[0], $exponotif[1], $exponotif[2]")

                        if (legendChange) {
                            val isInvisible: Boolean = sensorDataSeries.all { it.title.isBlank() }
                            graphView?.legendRenderer?.isVisible = !isInvisible
                            // statement has the side-effect that the legend size gets recalculated
                            graphView?.legendRenderer?.textSize =
                                dip2pixel(LEGEND_TEXT_SIZE).toFloat()
                        }
                        plotData(exponotif)
                    }
                }
                else -> super.handleMessage(msg)
            }
        }
    }

    // returns true if there was an update
    private fun updateLegend(i: Int, string: String): Boolean {
        if (sensorDataSeries[i].title != string) {
            sensorDataSeries[i].title = string
            sensorDataSeries[i].isInLegend = string.isNotBlank()
            return true
        }
        return false
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d(TAG, "onCreateView")
        exponotifOscKeys = Array(MAX_EXPONOTIFS) {
            getString(R.string.exponotif_osc_key) +'.' + it.toString() }
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_exponotif, container, false)
    }


     // called from super onViewCreated
     override fun setupGraphView(speedSetting: Int) {
         super.setupGraphView(speedSetting)
         graphView?.legendRenderer?.isVisible = false

         for (i:Int in sensorDataSeries.indices) {
             sensorDataSeries[i].title = ""
             sensorDataSeries[i].isInLegend = false
             sensorDataSeries[i].size = dip2pixel(PLOT_SHAPE_SIZE).toFloat()
         }

         sensorDataSeries[0].color = getThemeColor(context, "colorAccent")
         sensorDataSeries[1].color = getThemeColor(context, "colorPrimary")
         sensorDataSeries[2].color = COLOR_PLOT3.toInt() // orange
         sensorDataSeries[3].color = COLOR_PLOT4.toInt() // red
         sensorDataSeries[4].color = getColor(requireContext(), R.color.colorBright)
         sensorDataSeries[5].color = COLOR_PLOT6.toInt() // pink
         sensorDataSeries[6].color = COLOR_PLOT7.toInt() // green
         sensorDataSeries[7].color = COLOR_PLOT8.toInt() // yellow
         sensorDataSeries[8].color = COLOR_PLOT9.toInt() // blue
         sensorDataSeries[9].color = COLOR_PLOT10.toInt() // white


         for (i:Int in sensorDataSeries.indices)
            sensorDataSeries[i].strokeWidth = 5f

         sensorDataSeries[3].shape = PointsGraphSeries.Shape.TRIANGLE
         sensorDataSeries[4].strokeWidth = 7f // more width due to weak contrast
         sensorDataSeries[4].shape = PointsGraphSeries.Shape.TRIANGLE
         sensorDataSeries[5].shape = PointsGraphSeries.Shape.TRIANGLE
         sensorDataSeries[6].shape = PointsGraphSeries.Shape.RECTANGLE
         sensorDataSeries[7].shape = PointsGraphSeries.Shape.RECTANGLE
         sensorDataSeries[8].shape = PointsGraphSeries.Shape.RECTANGLE

         for (sds in sensorDataSeries) {
             graphView?.addSeries(sds)
         }

        // set time axis label formatter
        graphView?.title = resources.getString(R.string.exponotif_title)
    }


    fun plotData(values: FloatArray) {
        val stepCounter = Date(System.currentTimeMillis())
        // have an invisible data point to keep the chart scrolling
        // NOT THREAD SAFE
        activity?.runOnUiThread {
            invisibleSeries.appendData(DataPoint(stepCounter, 0.0), true, DATA_LEN)
            for (i in values.indices) {
                if (!values[i].isNaN())
                    sensorDataSeries[i].appendData(
                        DataPoint(stepCounter, values[i].toDouble()),
                        true,
                        DATA_LEN
                    )
            }
        }
    }



}