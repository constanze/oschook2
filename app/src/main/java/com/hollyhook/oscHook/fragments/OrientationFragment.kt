package com.hollyhook.oscHook.fragments

import android.os.*
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hollyhook.oscHook.*
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.PointsGraphSeries
import java.util.*

/**
 * A [PlotFragment] subclass for Orientation
 */
class OrientationFragment : PlotFragment(
    OscHookService.MSG_REGISTER_ORIENTATION_CLIENT,
    OscHookService.MSG_UNREGISTER_ORIENTATION_CLIENT
) {

    override var TAG = "oscHook.OrientationFragment"
    private lateinit var orientationOscKeys: Array<String>
    private lateinit var sensorDataSeries: Array<PointsGraphSeries<DataPoint>>

    // initializer block
    init {
        setIncomingHandler(IncomingHandler())
    }

    // called by the service
    inner class IncomingHandler : Handler() {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                OscHookService.MSG_ORIENTATION -> {
                    if (msg.peekData() == null)
                        plotData() // just scroll a bit
                    else {
                        // re-using the preferences key here
                        val orientation = FloatArray(3)
                        for (i in orientationOscKeys.indices) {
                            val key = orientationOscKeys[i]
                            try {
                                orientation[i] = msg.data.getFloat(key)
                            } catch (ex: IllegalStateException) {
                                // the fragment might be dead already
                            }
                        }
                        // Log.v(TAG, "Orientation $orientation[0], $orientation[1], $orientation[2]")
                        plotData(orientation)
                    }
                }
                else -> super.handleMessage(msg)
            }
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d(TAG, "onCreateView")
        orientationOscKeys = arrayOf(getString(R.string.orientation_alpha_osc_key),
            getString(R.string.orientation_beta_osc_key),
            getString(R.string.orientation_gamma_osc_key))

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_orientation, container, false)
    }


     // called from super onViewCreated
     override fun setupGraphView(speedSetting: Int) {
         super.setupGraphView(speedSetting)
         graphView?.legendRenderer?.isVisible = true

         sensorDataSeries = arrayOf( // x,y,z
            PointsGraphSeries<DataPoint>(),
            PointsGraphSeries<DataPoint>(),
            PointsGraphSeries<DataPoint>()
         )

         sensorDataSeries[0].title = getString(R.string.alpha)
         sensorDataSeries[1].title = getString(R.string.beta)
         sensorDataSeries[2].title = getString(R.string.gamma)
         sensorDataSeries[0].color = getThemeColor(context, "colorAccent")
         sensorDataSeries[1].color = getThemeColor(context, "colorPrimary")
         sensorDataSeries[2].color = COLOR_PLOT3.toInt() // orange

         for (sds in sensorDataSeries) {
             sds.isInLegend = true
             sds.strokeWidth = if (speedSetting == 0) 3f else 1f
             sds.size = dip2pixel(PLOT_SHAPE_SIZE).toFloat()
             graphView?.addSeries(sds)
         }

        // set time axis label formatter
        graphView?.title = resources.getString(R.string.orientation_title)
    }


    fun plotData(values: FloatArray) {
        val stepCounter = Date(System.currentTimeMillis())

        // have an invisible data point to keep the chart scrolling
        // NOT THREAD SAFE
        activity?.runOnUiThread {
            invisibleSeries.appendData(DataPoint(stepCounter, 0.0), true, DATA_LEN)
            for (i in values.indices)
                sensorDataSeries[i].appendData(DataPoint(stepCounter, values[i].toDouble()), true, DATA_LEN)
        }
    }
}