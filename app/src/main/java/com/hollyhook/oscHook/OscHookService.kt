package com.hollyhook.oscHook

import android.annotation.SuppressLint
import android.app.*
import android.app.NotificationManager.IMPORTANCE_LOW
import android.content.Intent
import android.content.SharedPreferences
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.location.LocationManager
import android.net.wifi.WifiManager
import android.os.*
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationCompat.Builder
import androidx.core.app.NotificationCompat.PRIORITY_LOW
import androidx.core.app.NotificationManagerCompat
import com.hollyhook.oscHook.spl.Recorder
import com.illposed.osc.OSCMessage
import com.illposed.osc.transport.udp.OSCPortOut
import org.altbeacon.beacon.*
import java.net.InetAddress
import java.util.*
import kotlin.math.PI
import kotlin.math.asin
import kotlin.math.atan2
import kotlin.math.min


class OscHookService : Service(),
    SensorEventListener,
    BeaconConsumer,
    SharedPreferences.OnSharedPreferenceChangeListener {

    private val TAG = "OscHookService"
    private var timer: Timer? = null
    private var counter = 0
    private var resetCounter = 0
    private var runningAsForegroundService = false

    /** OSC sending object */
    private var oscSender: OSCPortOut? = null
    private var oscAddressChanged = false
    private var combineOsc: Boolean = COMBINE_OSC_DEFAULT

    private lateinit var sensorManager: SensorManager
    private lateinit var beaconManager: BeaconManager
    private lateinit var locationManager: LocationManager

    private var updateRate: Int = SensorManager.SENSOR_DELAY_UI

    // setup for common part of the sensors
    private val light = SensorSetup.Light(this)
    private val linacc = SensorSetup.Linacc(this)
    private val accelerometer = SensorSetup.Accelerometer(this)
    private val orientation = SensorSetup.Orientation(this)
    private val beacon = SensorSetup.Beacon(this)
    private val exponotif = SensorSetup.Exponotif(this)
    private val audio = SensorSetup.Audio(this)
    private val satnav = SensorSetup.Satnav(this)

    // for iterating over the sensors
    private val mySensors = listOf(
        light, linacc, accelerometer, orientation, beacon, exponotif, satnav, audio
    )

    private var exponotifTrackingTime: Int = DEFAULT_TRACKING_TIME

    // Target we publish for clients to send messages to IncomingHandler.
    private val registrationMessenger: Messenger =
        Messenger(IncomingHandler(this))

    private val settingWakelock = true
    private var wakelock: PowerManager.WakeLock? = null
    private var wifiLock: WifiManager.WifiLock? = null

    /** Preferences saving, like port settings etc */
    private var sharedPref: SharedPreferences? = null

    private var positionHandler: PositionHandler? = null

    /* called for both, bound service and foreground service.
     * therefore the sensor initialization must be here
     */
    @SuppressLint("WakelockTimeout")
    override fun onCreate() {
        super.onCreate()
        Log.d(TAG, "onCreate")

        // check preference for stored selected data
        restoreSharedPreferences()
        Log.v(TAG, "registered for shared preference change")
        sharedPref?.registerOnSharedPreferenceChangeListener(this)

        oscSender = createOscSender()

        sensorManager = getSystemService(SENSOR_SERVICE) as SensorManager
        light.sensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT)
        linacc.sensor = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION)
        accelerometer.sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)

        val rvl = sensorManager.getSensorList(Sensor.TYPE_ROTATION_VECTOR)
        if (rvl.isEmpty())
            // untested
            orientation.sensor = sensorManager.getDefaultSensor(Sensor.TYPE_GAME_ROTATION_VECTOR)
        else
            orientation.sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR)

        // according to https://developer.android.com/guide/topics/sensors/sensors_overview
        // SensorManager.SENSOR_DELAY_NORMAL approx 200ms
        // SensorManager.SENSOR_DELAY_UI 60ms
        // SensorManager.SENSOR_DELAY_GAME 20ms

        beaconManager = BeaconManager.getInstanceForApplication(this)

        // For the above foreground scanning service to be useful, you need to disable
        // JobScheduler-based scans (used on Android 8+) and set a fast background scan
        // cycle that would otherwise be disallowed by the operating system.
        //
        if (!beaconManager.isAnyConsumerBound)
            beaconManager.setEnableScheduledScanJobs(false)
        beaconManager.isRegionStatePersistenceEnabled = false
        beaconManager.foregroundBetweenScanPeriod = 0
        beaconManager.foregroundScanPeriod = 1100

        //BeaconManager.setDebug(true) //gets very noisy
        beaconManager.applySettings()

        // audio
        recorderInstance = Recorder(recordingHandler)

        // GPS, Galileo etc.
        // use the old school location manager, not the fused one, to avoid integrating
        // Google play services
        locationManager = getSystemService(LOCATION_SERVICE) as LocationManager;
        positionHandler = PositionHandler(locationManager, satnav.measuredValues, satnav.useOffsets);

        // kick off main heartbeat
        val period =
            if (updateRate == SensorManager.SENSOR_DELAY_GAME) 20L else 60L

        // also for SENSOR_DELAY_NORMAL (200ms) we use the 60ms, in order to
        // have the UI more smoothly scrolling.

        timer = Timer()
        timer!!.scheduleAtFixedRate(object : TimerTask() {
            override fun run() {
                onTimerTick()
            }
        }, 0, period)
        isRunning = true

        if (settingWakelock && wakelock == null) {
            val pm: PowerManager = getSystemService(POWER_SERVICE) as PowerManager
            wakelock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "$TAG:pwl")

            val wm = applicationContext.getSystemService(WIFI_SERVICE) as WifiManager
            wifiLock = wm.createWifiLock(WifiManager.WIFI_MODE_FULL_HIGH_PERF, "$TAG:wwl")

            Log.i(TAG, "requesting partial wake lock and wifi lock")
            wakelock?.acquire() // to be released on destroying the service
            wifiLock?.acquire()

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val b = pm.isIgnoringBatteryOptimizations(packageName)
                Log.i(TAG, "$packageName isIgnoringBatteryOptimizations: $b")
            }
        }
    }

    private fun updateSensorListenersRegister() {

        fun update(sensor: Sensor?, stat: Boolean, client: Messenger?, name: String) {
            if ((stat && runningAsForegroundService) || client != null) {
                // either OSC is requested in service mode, or a client is available
                // register to the sensor
                Log.d(TAG, "register for $name")
                sensorManager.registerListener(this, sensor, updateRate)
            } else {
                // say goodbye
                Log.d(TAG, "unregister for $name")
                sensorManager.unregisterListener(this, sensor)
            }
        }

        for (ss in mySensors) {
            if (ss != beacon && ss != exponotif && ss != audio) {
                update(ss.sensor, ss.on, ss.client, ss.name)
            }
        }

        // beacon use bind / unbind instead of sensorManager
        if ((runningAsForegroundService && (exponotif.on || beacon.on))
                    || exponotif.client != null || beacon.client != null)
            bindBeacons()
        else
            unbindBeacons()

        // recording has its own interface as well
        if ((runningAsForegroundService && satnav.on)
            || satnav.client != null )
            positionHandler!!.register()
        else
            positionHandler!!.unregister()


        // recording has its own interface as well
        if ((runningAsForegroundService && audio.on)
            || audio.client != null )
            startRecording()
        else
            stopRecording()

        // check we can leave
        val isActive = isClientConnected()

        // nothing more to do, the last client is disconnected
        if (!runningAsForegroundService && !isActive) {
            Log.v(TAG, "unregistered last client. stopping")
            stopMyService()
        }
    }

    // the fragment is connecting
    override fun onBind(intent: Intent): IBinder? {
        Log.d(TAG, "onBind")
        return registrationMessenger.binder
    }

    private fun isClientConnected(): Boolean {
        for (ss in mySensors) {
            if (ss.client != null) return true
        }
        return false
    }


    // this is called from the activity floating button, or on service quit
    // the fragments bind/unbind from the service, which is not coming here

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d(TAG, "onStartCommand. Received start id $startId intent $intent?.action")
        return if (intent != null && intent.action == OSCHOOK_ACTION_START_SERVICE) {
            runningAsForegroundService = true
            updateSensorListenersRegister()

            // make location transparent since we have the background location permission
            val textId = if (locationOsc()) // needs preferences
                R.string.transmitting_location
            else
                R.string.transmitting_sensor_data

            // start the service
            startForeground(NOTIFICATION_ID, buildNotification(textId))
            START_STICKY // run until explicitly stopped.
        } else {
            runningAsForegroundService = false
            updateSensorListenersRegister()
            stopMyService()
            START_NOT_STICKY
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        timer?.cancel()
        sensorManager.unregisterListener(this)
        unbindBeacons()
        stopRecording()
        counter = 0
        Log.d(TAG, "onDestroy. Service Stopped.")
        isRunning = false
        oscSender?.close()

        if (wakelock != null) {
            Log.i(TAG, "release wakelock")
            wakelock!!.release()
            wifiLock!!.release()
            wakelock = null
        }
    }

    private fun stopMyService() {
        // take away the notification.
        NotificationManagerCompat.from(this).apply {
            cancel(NOTIFICATION_ID)
        }

        stopForeground(true)
        stopSelf() // will call onDestroy, where the rest of the de-init is done
    }

    //--- Notifications -------------------------------------------------------

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(): String {
        val channelName = getString(R.string.app_name)
        val visibility = Notification.VISIBILITY_PUBLIC
        val chan = NotificationChannel(TAG, channelName, IMPORTANCE_LOW)

        chan.lockscreenVisibility = visibility
        chan.enableVibration(false)

        val service = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(chan)
        return TAG
    }



    /**
     * create a notification for the foreground service. when the notification
     * is tapped on, re-start the MainActivity
     */
    private fun buildNotification(text: String): Notification {
        val channelId =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                createNotificationChannel()
            } else {
                // In earlier version channels are not available
                ""
            }
        val intent = Intent(this, OscHookService::class.java)
        intent.action = OSCHOOK_ACTION_CLOSE_SERVICE // will end myself

        val closeIntent = PendingIntent.getService(
            this,
            1, intent, PendingIntent.FLAG_UPDATE_CURRENT
        )

        // intent for when notification is tapped, restart activity
        val notificationIntent = Intent(applicationContext, MainActivity::class.java)
        notificationIntent.action = "oschook.action.main" // A string containing the action name
        notificationIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        val contentPendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0)

        val n = Builder(this, channelId)
            .setContentTitle(getString(R.string.app_name))
            .setContentText(text)
            .setSmallIcon(R.drawable.hook24)
            .setContentIntent(contentPendingIntent)
            .setPriority(PRIORITY_LOW)
            //.setLargeIcon(bmp)
            .setOngoing(true) // which is the default for a foreground service
            .addAction(0, getString(R.string.action_quit), closeIntent)

        return n.build()
    }

    private fun buildNotification(textID: Int): Notification {
        return buildNotification(getString(textID))
    }

    private fun updateNotification(text: String) {
        val pn = buildNotification(text)
        NotificationManagerCompat.from(this).apply {
            notify(NOTIFICATION_ID, pn)
        }
    }


        //--- sensor data output handling ----------------------------------------

    /**
     * periodically called in sync with the setting in the app
     */
    private fun onTimerTick() {
        //Log.v(TAG, "onTimerTick. $counter")
        if (oscAddressChanged) {
            if (oscSender != null) {
                oscSender!!.close()
                oscSender = null
                return // give one tick time to close the udp
            } else {
                // try again to open
                oscSender = createOscSender()
                oscAddressChanged = false
            }
        }

        val resultList = ArrayList<Any>()
        try {
            processAndSend(light, resultList)
            processAndSend(linacc, resultList)
            processAndSend(accelerometer, resultList)
            processAndSend(orientation, resultList)
            processAndSendBLE(beacon, resultList)
            processAndSendBLE(exponotif, resultList)
            processAndSendSatnav(resultList)
            processAndSendAudio(resultList)


            if (combineOsc && resultList.isNotEmpty()) {
                // so we collected all values in result list
                // append a time stamp
                resultList.add(System.currentTimeMillis())
                val msg = OSCMessage(COMBINE_OSC_ADDRESS_DEFAULT, resultList)
                sendOsc(msg)
            }

            // every minute or so look if all is ok
            if (counter % 1000 == 0 && isClientConnected()) {
//                updateNotification("sent $counter resets $resetCounter") // keeps the service alive
                if (oscSender == null) {
                    Log.w(TAG, "recreating OSC sender in timer tick")
                    oscSender = createOscSender()
                    resetCounter++
                    counter = 0
                }
            }
            counter += 1

        } catch (t: Throwable) {
            // always ultimately catch all exceptions in timer tasks.
            Log.e("TimerTick", "Timer Tick Failed.", t)
        }

    }


    /**
     * this function is for all float sensors. do slide and scaling and send
     * the result to the client and the network. called from timer tick!
     */
    private fun processAndSend(ss: SensorSetup, result: ArrayList<Any>) {
        if (updateRate != SensorManager.SENSOR_DELAY_NORMAL || counter % 3 == 0) {
            ss.process()

            // client output. skip every second measurement in fast mode
            if (!(updateRate == SensorManager.SENSOR_DELAY_GAME && counter %2 == 0))
                ss.sendMessageToClient()

            // OSC output
            if (oscSender != null && ss.on)
                for (i in ss.oscAddresses.indices) {
                    val oscAddress = ss.getOscAddress(i)
                    if (!combineOsc)
                        sendOscFloat(oscAddress, ss.outValues[i])
                    else {
                        if (oscAddress.isNotBlank())
                            result.add(ss.outValues[i])
                    }
                }
        } else ss.sendEmptyMessageToClient()
    }

    private val FROM_RADS_TO_DEG = 180.0/PI

    override fun onSensorChanged(event: SensorEvent) {
        for (ss in mySensors) {
             if (event.sensor == ss.sensor) {
                 if (ss == orientation) {
                     // calculate orientation from rotation vector
                     val rotationMatrix = FloatArray(9)
                     SensorManager.getRotationMatrixFromVector(rotationMatrix, event.values)
                     val orientationAngles = DoubleArray(3)
                     computeOrientationFromRotationMatrix(rotationMatrix, orientationAngles)

                     for (i in orientation.measuredValues.indices)
                         orientation.measuredValues[i] = (orientationAngles[i] * FROM_RADS_TO_DEG).toFloat()

                     Log.v(
                         TAG,
                         "onSensorChanged orientation " + orientation.measuredValues.toString()
                     )

                 } else {
                     // all other sensors it is a simple copy
                     for (i in 0 until min(ss.dimension, event.values.size))
                         ss.measuredValues[i] = event.values[i]
                 }
             }
         }
    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {
        Log.d(TAG, "onAccuracyChanged " + p0?.toString())
        // well ...
    }


    // copied from chromium

    /**
     * Returns orientation angles from a rotation matrix, such that the angles are according
     * to spec [http://dev.w3.org/geo/api/spec-source-orientation.html].
     *
     * It is assumed the rotation matrix transforms a 3D column vector from device coordinate system
     * to the world's coordinate system, as e.g. computed by {@see SensorManager.getRotationMatrix}.
     *
     * In particular we compute the decomposition of a given rotation matrix R such that <br></br>
     * R = Rz(alpha) * Rx(beta) * Ry(gamma), <br></br>
     * where Rz, Rx and Ry are rotation matrices around Z, X and Y axes in the world coordinate
     * reference frame respectively. The reference frame consists of three orthogonal axes X, Y, Z
     * where X points East, Y points north and Z points upwards perpendicular to the ground plane.
     * The computed angles alpha, beta and gamma are in radians and clockwise-positive when viewed
     * along the positive direction of the corresponding axis. Except for the special case when the
     * beta angle is +-pi/2 these angles uniquely define the orientation of a mobile device in 3D
     * space. The alpha-beta-gamma representation resembles the yaw-pitch-roll convention used in
     * vehicle dynamics, however it does not exactly match it. One of the differences is that the
     * 'pitch' angle beta is allowed to be within [-pi, pi). A mobile device with pitch angle
     * greater than pi/2 could correspond to a user lying down and looking upward at the screen.
     *
     * Upon return the array values is filled with the result,
     *
     *  * values[0]: rotation around the Z axis, alpha in [0, 2*pi)
     *  * values[1]: rotation around the X axis, beta in [-pi, pi)
     *  * values[2]: rotation around the Y axis, gamma in [-pi/2, pi/2)
     *
     * @param R
     * a 3x3 rotation matrix {@see SensorManager.getRotationMatrix}.
     *
     * @param values
     * an array of 3 doubles to hold the result.
     *
     * @return the array values passed as argument.
     */
    private fun computeOrientationFromRotationMatrix(
        R: FloatArray,
        values: DoubleArray
    ): DoubleArray {
        /*
         * 3x3 (length=9) case:
         *   /  R[ 0]   R[ 1]   R[ 2]  \
         *   |  R[ 3]   R[ 4]   R[ 5]  |
         *   \  R[ 6]   R[ 7]   R[ 8]  /
         *
         */
        if (R.size != 9) return values
        if (R[8] > 0) {  // cos(beta) > 0
            values[0] = atan2((-R[1]).toDouble(), R[4].toDouble())
            values[1] = asin(R[7].toDouble()) // beta (-pi/2, pi/2)
            values[2] = atan2((-R[6]).toDouble(), R[8].toDouble()) // gamma (-pi/2, pi/2)
        } else if (R[8] < 0) {  // cos(beta) < 0
            values[0] = atan2(R[1].toDouble(), (-R[4]).toDouble())
            values[1] = -asin(R[7].toDouble())
            values[1] += if (values[1] >= 0) -Math.PI else Math.PI // beta [-pi,-pi/2) U (pi/2,pi)
            values[2] = atan2(R[6].toDouble(), (-R[8]).toDouble()) // gamma (-pi/2, pi/2)
        } else { // R[8] == 0
            if (R[6] > 0) {  // cos(gamma) == 0, cos(beta) > 0
                values[0] = atan2((-R[1]).toDouble(), R[4].toDouble())
                values[1] = asin(R[7].toDouble()) // beta [-pi/2, pi/2]
                values[2] = -Math.PI / 2 // gamma = -pi/2
            } else if (R[6] < 0) { // cos(gamma) == 0, cos(beta) < 0
                values[0] = atan2(R[1].toDouble(), (-R[4]).toDouble())
                values[1] = -asin(R[7].toDouble())
                values[1] += if (values[1] >= 0) -Math.PI else Math.PI // beta [-pi,-pi/2) U (pi/2,pi)
                values[2] = -Math.PI / 2 // gamma = -pi/2
            } else { // R[6] == 0, cos(beta) == 0
                // gimbal lock discontinuity
                values[0] = atan2(R[3].toDouble(), R[0].toDouble())
                values[1] = if (R[7] > 0) Math.PI / 2 else -Math.PI / 2 // beta = +-pi/2
                values[2] = 0.0 // gamma = 0
            }
        }
        // alpha is in [-pi, pi], make sure it is in [0, 2*pi).
        if (values[0] < 0) values[0] += 2 * Math.PI // alpha [0, 2*pi)
        return values
    }

    //--- Toasts -----------------------------------------------------------

    // only show the toast when we have an ui
    private fun showToast(str: String) {
        if (isClientConnected()) {
            val handler = Handler(Looper.getMainLooper())
            handler.post {
                Toast.makeText(
                    applicationContext, str, Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    // only show the toast when we have an ui
    private fun showToast(strId: Int) {
        if (isClientConnected()) {
            val handler = Handler(Looper.getMainLooper())
            handler.post {
                Toast.makeText(
                    applicationContext, strId, Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    //--- OSC helpers ---------------------------------------------------------

    private fun createOscSender(): OSCPortOut? {
        var s:OSCPortOut? = null
        val host: InetAddress

        // restore ip nd port
        val ip: String? = sharedPref!!.getString(
            getString(R.string.ip_key), IP_DEFAULT
        )
        // likewise for port
        val port: Int = sharedPref!!.getInt(
            getString(R.string.port_key), PORT_DEFAULT
        )

        try {
            host = InetAddress.getByName(ip)
            s = OSCPortOut(host, port)
            Log.i(TAG, "OSC Port Out created")
        } catch (e: Exception) {
            val msg = if (e.message != null) e.message else e.toString()
            updateNotification(msg!!)
            showToast(msg)
            Log.w(TAG, e.toString())
        }
        return s
    }

    private val floatArgList: ArrayList<Float> = arrayListOf(0f)

    // send single OSC message which just contains a single float
    private fun sendOscFloat(address: String, value: Float) {
        if (address.isNotBlank() && oscSender != null) {
            Log.v(TAG, "osc $address $value")
            floatArgList[0] = value
            val msg = OSCMessage(address, floatArgList)
            sendOsc(msg)
        }
    }

    private fun sendOsc(msg: OSCMessage) {
        try {
            oscSender?.send(msg)
        } catch (e: Exception) {
            Log.w(TAG, "error sending ${msg.address} " + (e.message ?: e.toString()))
            updateNotification(e.message ?: e.toString())
            showToast(e.message ?: e.toString())
            Log.w(TAG, "now closing failed OSC sender")
            oscSender?.close()
            oscSender = null // try to reopen it in the next tick
        }
    }


    //--- beacons ---------------------------------------------------------

    private val myRegion = Region(TAG, null, null, null)

    // connect to beacons library
    // check for bt availability and permissions has been done in the application
    private fun bindBeacons() {
        Log.i(TAG, "bindBeacons")
        beaconManager.beaconParsers.clear()
        for (layout: String in arrayOf(
            IBEACON,
            ALTBEACON, ALTBEACON2,
            EDDYSTONE_TLM, EDDYSTONE_UID, EDDYSTONE_URL,
            EXPOSURE
        )) {
            beaconManager.beaconParsers.add(BeaconParser().setBeaconLayout(layout))
        }
        Log.i(TAG, "binding to beacon manager: $this")
        beaconManager.bind(this)
    }

    // unbind only if neither beacons nor exponotifs are wanted
    private fun unbindBeacons() {
        Log.i(TAG, "unbindBeacons")
        if (!beaconManager.isBound(this)) {
            Log.w(TAG, "not bound to beacon manager $this")
            //return
        }

        if (beacon.client == null && exponotif.client == null &&
                !runningAsForegroundService) {
            Log.i(TAG, "unbind beacon manager")
            beaconManager.stopRangingBeaconsInRegion(myRegion)
            beaconManager.unbind(this)
        }
    }

    // beacons library calls this
    // same function handles both beacons and exposure notifications
    override fun onBeaconServiceConnect() {
        Log.d(TAG, "onBeaconServiceConnect")
        val rangeNotifier = RangeNotifier { beacons, _ ->
            if (beacons.isNotEmpty()) {
                Log.d(
                    TAG,
                    "rangeNotifier called with beacon count:  " + beacons.size
                )
                val now = System.currentTimeMillis()
                val i = beacons.iterator()
                while (i.hasNext()) {
                    val nextBeacon = i.next()
                    var myList: Array<AgingBeacon?>
                    var ss: SensorSetup
                    if (nextBeacon.serviceUuid == ExposureNotificationServiceUuid) {
                        ss = exponotif
                        myList = exponotif.exponotifList
                    } else {
                        ss = beacon
                        myList = beacon.beaconList
                    }

                    val foundInList = ss.findBeaconInList(nextBeacon, myList, now)
                    if (!foundInList) {
                        // a new beacon showed up, save at next free spot
                        for (b in myList.indices) {
                            if (myList[b] == null) {
                                Log.d(
                                    TAG,
                                    "a new ${ss.name} for my list: ${nextBeacon.bluetoothAddress}"
                                )

                                myList[b] = AgingBeacon(
                                    nextBeacon.bluetoothAddress,
                                    now,
                                    nextBeacon.distance.toFloat()
                                )
                                break
                            }
                        }
                    }
                }
            }
            else
                Log.d(TAG, "rangeNotifier called with empty beacon list")
        }
        try {
            beaconManager.startRangingBeaconsInRegion(myRegion)
            beaconManager.addRangeNotifier(rangeNotifier)
        } catch (e: RemoteException) {
            Log.e(TAG, "failed starting beacon lib " + e.message)
        }
    }

    /**
     * this function is within scope of timer tick. if there is a client
     * we send something each timer tick. However, only if there is a fresh
     * measurement we send actual data. All other values are NaN. OSC
     * is only send on measurement received.
     */
    private fun processAndSendBLE(ss: SensorSetup, result: ArrayList<Any>) {
        if (updateRate != SensorManager.SENSOR_DELAY_NORMAL || counter % 3 == 0) {
            ss.process()
            ss.sendMessageToClient()

            // OSC output.
            if (oscSender != null && ss.on)
                for (i in ss.oscAddresses.indices) {
                    if (!ss.outValues[i].isNaN()) {
                        // we send only if we have received a measurement.
                        val oscAddress = ss.getOscAddress(i)
                        if (!combineOsc)
                            sendOscFloat(oscAddress, ss.outValues[i])
                        else {
                            if (oscAddress.isNotBlank())
                                result.add(ss.outValues[i])
                        }
                    }
                }

            // timeout handling for over aged beacons
            ss.postProcess(if (ss == beacon) DEFAULT_TRACKING_TIME else exponotifTrackingTime)

        } else
            ss.sendEmptyMessageToClient()
    }


    /**
     * this function is within scope of timer tick. if there is a client
     * we send an empty message each timer tick. However, only if there is a fresh
     * measurement we send actual data. All other values are NaN. OSC
     * is only if data is available
     */
    private fun processAndSendSatnav(result: ArrayList<Any>) =
// we send only if we have received a measurement.
        if ((updateRate != SensorManager.SENSOR_DELAY_NORMAL || counter % 3 == 0)
            && !satnav.measuredValues[0].isNaN()
        ) {
            satnav.process()
            satnav.sendMessageToClient()

            // OSC output.
            if (oscSender != null && satnav.on) {
                for (i in satnav.oscAddresses.indices) {
                    val oscAddress = satnav.getOscAddress(i)
                    if (!combineOsc)
                        sendOscFloat(oscAddress, satnav.outValues[i])
                    else {
                        if (oscAddress.isNotBlank())
                            result.add(satnav.outValues[i])
                    }
                }
            }
            satnav.postProcess(0)
        } else
            satnav.sendEmptyMessageToClient()

    //--- Audio ---------------------------------------------------------

    private var recordingThread: Thread? = null
    private var recorderInstance: Recorder? = null

    /* Handler for displaying messages from recording thread */
    private var recordingHandler: Handler = RecordingHandler()

    inner class RecordingHandler : Handler() {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                MSG_RECORDING -> {
                    val spl: Float = msg.data.getFloat("spl")
                    val f: Float = msg.data.getFloat("frequency")
                    //Log.v(TAG, "spl $spl dB, $f Hz")
                    if (spl == 0f) { // danger
                        audio.measuredValues[0] = Float.NaN
                        audio.measuredValues[1] = Float.NaN
                    } else {
                        audio.measuredValues[0] = spl
                        audio.measuredValues[1] = f
                    }
                }
                MSG_RECORDING_FAIL -> {
                    showToast(R.string.error_recording)
                    recorderInstance = null
                }
                else -> super.handleMessage(msg)
            }
        }
    }

    private fun startRecording() {
        Log.d(TAG, "start recording")
        if (recorderInstance == null) return
        recorderInstance!!.isRecording = true
        recordingThread = Thread(recorderInstance)
        if (recordingThread == null)
            Log.e(TAG, "couldn't get a thread for recording")
        else
            recordingThread!!.start()
    }

    private fun stopRecording() {
        Log.d(TAG, "stop recording")
        if (recorderInstance != null) {
            recorderInstance!!.isRecording = false
        }
    }


    // called from timer tick
    // only the first value is scaled.
    private fun processAndSendAudio(result: ArrayList<Any>) {
        if (updateRate != SensorManager.SENSOR_DELAY_NORMAL || counter % 3 == 0) {
            audio.process()

            // client output. skip every other measurement in fast mode
            if (!(updateRate == SensorManager.SENSOR_DELAY_GAME && counter % 3 == 0))
                audio.sendMessageToClient()

            if (audio.on) {
                // send osc
                for (i in audio.oscAddresses.indices) {
                    val oscAddress = audio.getOscAddress(i)
                    if (!combineOsc)
                        sendOscFloat(oscAddress, audio.outValues[i])
                    else {
                        if (oscAddress.isNotBlank())
                            result.add(audio.outValues[i])
                    }
                }
            }
        } else audio.sendEmptyMessageToClient()
    }



    //--- preferences ---------------------------------------------------------

    /**
     * port / ip has been changed by the user. stop and restart
     */
    override fun onSharedPreferenceChanged(pref: SharedPreferences?, key: String?) {
        Log.v(TAG, "onSharedPreferenceChanged")
        if (key == getString(R.string.port_key) || key == getString(R.string.ip_key)) {
            if (oscSender != null) {
                oscAddressChanged = true
                // timer?.cancel()
                // restart timer removed, due to
                /* java.lang.IllegalStateException: Timer already cancelled.
                at java.util.Timer.sched(Timer.java:404)
                at java.util.Timer.scheduleAtFixedRate(Timer.java:335)
                at com.hollyhook.oscHook.OscHookService.onSharedPreferenceChanged(OscHookService.kt:419)
                */
                //oscSender!!.close()
                //oscSender = null
            }
        } else if (key == getString(R.string.update_rate_key)) {
            restoreUpdateRate()

            if (audio.on && recorderInstance != null) {
                recorderInstance?.bufferSize = when(updateRate) {
                    SensorManager.SENSOR_DELAY_NORMAL -> 2048
                    SensorManager.SENSOR_DELAY_UI -> 1024
                    else -> 512
                }
            }

            // restart fixed timer cycle with new update rate
            // kick off main heartbeat
            val period =
                if (updateRate == SensorManager.SENSOR_DELAY_GAME) 20L else 60L
            Log.d(TAG, "update rate changed, new period is $period")

            if (isRunning) {
                timer?.cancel()
                timer = null /* I want a new one */
                timer = Timer()
                // re-scedule with new rate
                timer!!.scheduleAtFixedRate(object : TimerTask() {
                    override fun run() {
                        onTimerTick()
                    }
                }, 0, period)
            }
        } else // for any other preferences
            restoreSharedPreferences()

        fun isOnKey(k: String?): Boolean {
            if (k == null) return false
            for (ss in mySensors) {
                if (getString(ss.onKey) == k) return true
            }
            return false
        }

        if (isOnKey(key)) {
            val noOsc = noOsc()

            if (noOsc) runningAsForegroundService = false

            updateSensorListenersRegister()
            if (!isClientConnected() && noOsc)
                // nobody is connected anymore. nobody wants osc. we are done.
                stopMyService()

        }
    }

    private fun locationOsc(): Boolean {
        return sharedPref!!.getBoolean(getString(R.string.beacon_on_key), BEACON_ON_DEFAULT) ||
               sharedPref!!.getBoolean(getString(R.string.exponotif_on_key), EXPONOTIF_ON_DEFAULT) ||
               sharedPref!!.getBoolean(getString(R.string.satnav_on_key), SATNAV_ON_DEFAULT)
    }

    private fun noOsc(): Boolean {
        return !sharedPref!!.getBoolean(getString(R.string.light_on_key), LIGHT_ON_DEFAULT) &&
                !sharedPref!!.getBoolean(getString(R.string.linacc_on_key), LINACC_ON_DEFAULT) &&
                !sharedPref!!.getBoolean(
                    getString(R.string.orientation_on_key),
                    ORIENTATION_ON_DEFAULT
                ) &&
                !sharedPref!!.getBoolean(
                    getString(R.string.accelerometer_on_key),
                    ACCELEROMETER_ON_DEFAULT
                ) &&
                !sharedPref!!.getBoolean(getString(R.string.beacon_on_key), BEACON_ON_DEFAULT) &&
                !sharedPref!!.getBoolean(
                    getString(R.string.exponotif_on_key),
                    EXPONOTIF_ON_DEFAULT
                ) &&
                !sharedPref!!.getBoolean(getString(R.string.audio_on_key), AUDIO_ON_DEFAULT)
    }

    // read in all data specific settings in global variables
    private fun restoreSharedPreferences() {
        Log.d(TAG, "restoring preferences")
        sharedPref = getSharedPreferences(packageName, MODE_PRIVATE)

        val trackingIndex = sharedPref?.getInt(
            getString(R.string.tracking_time_key),
            EXPONOTIF_TRACKING_TIME_DEFAULT
        )

        // keep in sync with strings
        exponotifTrackingTime = when (trackingIndex) {
            0 -> 1000 * 5
            1 -> 1000 * 20
            2 -> 1000 * 60
            3 -> 1000 * 60 * 5
            4 -> 1000 * 60 * 20
            else -> DEFAULT_TRACKING_TIME
        }

        for (ss in mySensors)
            ss.restoreSharedPreferences(sharedPref!!)

        positionHandler?.useOffsets = satnav.useOffsets

        restoreUpdateRate()

        combineOsc = sharedPref!!.getBoolean(
            getString(R.string.osc_combined_key),
            COMBINE_OSC_DEFAULT
        )
    }

    private fun restoreUpdateRate() {
        val speedSetting = sharedPref?.getInt(getString(R.string.update_rate_key), 1)
        updateRate = when (speedSetting) {
            0 -> SensorManager.SENSOR_DELAY_NORMAL
            1 -> SensorManager.SENSOR_DELAY_UI
            2 -> SensorManager.SENSOR_DELAY_GAME
            else -> SensorManager.SENSOR_DELAY_UI
        }
    }

    companion object {
        var isRunning = false

        const val NOTIFICATION_ID = 10627

        // activity -> service
        const val MSG_REGISTER_LIGHT_CLIENT = 1
        const val MSG_UNREGISTER_LIGHT_CLIENT = 2
        const val MSG_REGISTER_LINACC_CLIENT = 3
        const val MSG_UNREGISTER_LINACC_CLIENT = 4
        const val MSG_REGISTER_ACCELEROMETER_CLIENT = 5
        const val MSG_UNREGISTER_ACCELEROMETER_CLIENT = 6
        const val MSG_REGISTER_ORIENTATION_CLIENT = 7
        const val MSG_UNREGISTER_ORIENTATION_CLIENT = 8
        const val MSG_REGISTER_BEACON_CLIENT = 9
        const val MSG_UNREGISTER_BEACON_CLIENT = 10
        const val MSG_REGISTER_EXPONOTIF_CLIENT = 11
        const val MSG_UNREGISTER_EXPONOTIF_CLIENT = 12
        const val MSG_REGISTER_SATNAV_CLIENT = 13
        const val MSG_UNREGISTER_SATNAV_CLIENT = 14
        const val MSG_REGISTER_AUDIO_CLIENT = 15
        const val MSG_UNREGISTER_AUDIO_CLIENT = 16

        const val MSG_SATNAV_RESET_ORIGIN = 20


        // service -> activity
        const val MSG_LIGHT = 101
        const val MSG_LINACC = 102
        const val MSG_ACCELEROMETER = 103
        const val MSG_ORIENTATION = 104
        const val MSG_BEACON = 105
        const val MSG_EXPONOTIF = 106
        const val MSG_SATNAV = 107
        const val MSG_AUDIO = 108

        // recorder thread -> service
        const val MSG_RECORDING = 200
        const val MSG_RECORDING_FAIL = 201

        const val OSCHOOK_ACTION_START_SERVICE = "OSCHOOK_ACTION_START_SERVICE"
        const val OSCHOOK_ACTION_CLOSE_SERVICE = "OSCHOOK_ACTION_CLOSE_SERVICE"

        // the activity wants to show some data
        internal class IncomingHandler(private val oscHookService: OscHookService) : Handler() {
            // Handler of incoming messages from clients.
            override fun handleMessage(msg: Message) {
                Log.d(oscHookService.TAG, "incoming Message $msg.what")
    
                for (ss in oscHookService.mySensors) {
                    if (ss.registerMsg == msg.what) {
                        ss.client = msg.replyTo
                        oscHookService.updateSensorListenersRegister()
                        return
                    }
                    if (ss.unregisterMsg == msg.what) {
                        ss.client = null
                        oscHookService.updateSensorListenersRegister()
                        return
                    }
                }

                // there is an extra message if the GPS origin needs to be reset
                // to the next received coordinate
                if (msg.what == MSG_SATNAV_RESET_ORIGIN) {
                    oscHookService.positionHandler?.startLocation = null
                }

                super.handleMessage(msg)
            }
        }
    }

}