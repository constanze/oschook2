package com.hollyhook.oscHook


import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.*
import android.provider.Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.hollyhook.oscHook.OscHookService.Companion.OSCHOOK_ACTION_START_SERVICE
import com.hollyhook.oscHook.dialogs.*

class MainActivity : AppCompatActivity() {
    private val TAG = "oscHook.MainActivity"

    /** Preferences saving, like port settings etc */
    internal var sharedPref: SharedPreferences? = null

    @SuppressLint("BatteryLife")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.v(TAG, "onCreate")
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))

        // button to go into background
        findViewById<FloatingActionButton>(R.id.fab).setOnClickListener { view ->
            val pm: PowerManager = getSystemService(Context.POWER_SERVICE) as PowerManager
            if (anySensorOn()) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                    !pm.isIgnoringBatteryOptimizations(packageName)
                ) {
                    // need users permission to empty the battery
                    val myIntent =
                        Intent(
                            ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS,
                            Uri.parse("package:${this.packageName}")
                        )
                    startActivityForResult(myIntent, REQUEST_IGNORE_BATTERY_OPTIMIZATIONS)
                } else {
                    startOscHookService(view)
                }
            } else {
                // no sensor on
                val toast = Toast.makeText(this, R.string.switch_sensor_on, Toast.LENGTH_LONG)
                toast.show()
            }
        }

        // check preference for stored selected context menu
        sharedPref = getSharedPreferences(packageName, Context.MODE_PRIVATE)
    }

    // show a snack bar and after 1 sec start the service
    private fun startOscHookService(view: View?) {
        val startIntent = Intent(applicationContext, OscHookService::class.java)
        startIntent.action = OSCHOOK_ACTION_START_SERVICE
        startService(startIntent)

        if (view != null) {
            Snackbar.make(view, getText(R.string.snackBar_sending), Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
            // show the snack bar a little longer
            Handler(Looper.getMainLooper()).postDelayed({
                finish() // I'm done, the service takes over
            }, 1000)
        } else
            finish() // the service takes over without delay

    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)

        val navFragment = supportFragmentManager.primaryNavigationFragment
        if (navFragment != null) {
            val navController: NavController = navFragment.findNavController()
            if (navController.currentDestination != null) {
                when (navController.currentDestination?.id) {
                    R.id.LightFragment ->
                        setMenuVisible(menu, R.id.action_light)
                    R.id.AccelerometerFragment ->
                        setMenuVisible(menu, R.id.action_accelerometer)
                    R.id.LinaccFragment ->
                        setMenuVisible(menu, R.id.action_linacc)
                    R.id.OrientationFragment ->
                        setMenuVisible(menu, R.id.action_orientation)
                    R.id.BeaconFragment ->
                        setMenuVisible(menu, R.id.action_beacon)
                    R.id.ExponotifFragment ->
                        setMenuVisible(menu, R.id.action_exponotif)
                    R.id.SatnavFragment ->
                        setMenuVisible(menu, R.id.action_satnav)
                    R.id.AudioFragment ->
                        setMenuVisible(menu, R.id.action_audio)
                }
            }
        }
        return true
    }

    // hide all menu entries besides of one (and the first one)
    private fun setMenuVisible(menu: Menu, action: Int) {
        for (i in 1 until menu.size()) {
            menu.getItem(i).isVisible =
                menu.getItem(i).itemId == action
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_host_ip -> {
                // Create an instance of the dialog fragment and show it
                val dialog = HostAndPortDialog()
                dialog.show(supportFragmentManager, "HostAndPortDialog")
                true
            }
            R.id.action_settings -> {
                val dialog = AdvancedSettingsDialog()
                dialog.show(supportFragmentManager, "AdvancedSettingsDialog")
                true
            }

            R.id.action_light -> {
                val dialog = LightDialog()
                dialog.show(supportFragmentManager, "LightSettings")
                true
            }
            R.id.action_accelerometer -> {
                val dialog = AccelerometerDialog()
                dialog.show(supportFragmentManager, "AccelerometerSettings")
                true
            }
            R.id.action_linacc -> {
                val dialog = LinaccDialog()
                dialog.show(supportFragmentManager, "LinaccSettings")
                true
            }
            R.id.action_orientation -> {
                val dialog = OrientationDialog()
                dialog.show(supportFragmentManager, "OrientationSettings")
                true
            }
            R.id.action_beacon -> {
                // open settings dialog
                val dialog = BeaconDialog()
                dialog.show(supportFragmentManager, "BeaconDialog")
                true
            }
            R.id.action_exponotif -> {
                // open settings dialog
                val dialog = ExponotifDialog()
                dialog.show(supportFragmentManager, "ExponotifDialog")
                true
            }
            R.id.action_satnav -> {
                // open settings dialog
                val dialog = SatnavDialog()
                dialog.show(supportFragmentManager, "SatnavDialog")
                true
            }
            R.id.action_audio -> {
                // open settings dialog
                val dialog = AudioDialog()
                dialog.show(supportFragmentManager, "AudioDialog")
                true
            }

            R.id.action_help -> {
                // start activity with help text
                val i = Intent(applicationContext!!, HelpActivity().javaClass)
                this.startActivity(i)
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun anySensorOn(): Boolean {
        return sharedPref!!.getBoolean(getString(R.string.light_on_key), LIGHT_ON_DEFAULT)
                || sharedPref!!.getBoolean(getString(R.string.linacc_on_key), LINACC_ON_DEFAULT)
                || sharedPref!!.getBoolean(
            getString(R.string.orientation_on_key),
            ORIENTATION_ON_DEFAULT
        )
                || sharedPref!!.getBoolean(
            getString(R.string.accelerometer_on_key),
            ACCELEROMETER_ON_DEFAULT
        )
                || sharedPref!!.getBoolean(getString(R.string.beacon_on_key), BEACON_ON_DEFAULT)
                || sharedPref!!.getBoolean(
            getString(R.string.exponotif_on_key),
            EXPONOTIF_ON_DEFAULT
        )
                || sharedPref!!.getBoolean(getString(R.string.satnav_on_key), SATNAV_ON_DEFAULT)
                || sharedPref!!.getBoolean(getString(R.string.audio_on_key), AUDIO_ON_DEFAULT)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IGNORE_BATTERY_OPTIMIZATIONS) {
            if (resultCode == Activity.RESULT_OK) {
                startOscHookService(findViewById(R.id.fab))
            } else {
                val builder = AlertDialog.Builder(this)
                builder.setTitle(R.string.battery_permission)
                builder.setMessage(R.string.battery_permission_denial)
                builder.setPositiveButton(android.R.string.ok, null)
                builder.setOnDismissListener {
                    it.dismiss()
                }
                builder.show()

            }
        }
    }
}