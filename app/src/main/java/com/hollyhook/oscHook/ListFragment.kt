package com.hollyhook.oscHook

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.ListActivity
import android.bluetooth.BluetoothManager
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Typeface
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.hollyhook.oscHook.dialogs.*
import android.content.Context.LOCATION_SERVICE


/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class ListFragment :
    Fragment(),
    SharedPreferences.OnSharedPreferenceChangeListener {
    var TAG = "com.hollyhook.oscHook.ListFragment"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.v(TAG, "onCreateView")

        // check preference for stored selected data
        val sharedPref = requireActivity().getSharedPreferences(
            requireActivity().packageName, Context.MODE_PRIVATE
        )
        sharedPref?.registerOnSharedPreferenceChangeListener(this)

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.v(TAG, "onViewCreated")
        super.onViewCreated(view, savedInstanceState)

        // load preferences to display enabled  list items in bold
        updateListItems(
            requireActivity().getSharedPreferences(
                requireActivity().packageName, Context.MODE_PRIVATE
            )
        )

        // setup light dialog
        var txt: TextView = view.findViewById(R.id.list_light) as TextView
        txt.setOnClickListener {
            findNavController().navigate(R.id.action_ListFragment_to_LightFragment)
        }
        txt.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent): Boolean {
                if (event.action == MotionEvent.ACTION_UP) {
                    if (event.rawX < txt.layout.width - txt.compoundPaddingEnd) {
                        // open light fragment
                        findNavController().navigate(R.id.action_ListFragment_to_LightFragment)
                    } else {
                        // open settings dialog
                        val dialog = LightDialog()
                        dialog.show(requireActivity().supportFragmentManager, "LightDialog")
                    }
                    return true
                }
                return false
            }
        })

        // linear acceleration
        txt = view.findViewById(R.id.list_linacc) as TextView
        txt.setOnClickListener {
            findNavController().navigate(R.id.action_ListFragment_to_LinaccFragment)
        }
        txt.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent): Boolean {
                if (event.action == MotionEvent.ACTION_UP) {
                    if (event.rawX < txt.layout.width - txt.compoundPaddingEnd) {
                        // open fragment
                        findNavController().navigate(R.id.action_ListFragment_to_LinaccFragment)
                    } else {
                        // open settings dialog
                        val dialog = LinaccDialog()
                        dialog.show(requireActivity().supportFragmentManager, "LinaccDialog")
                    }
                    return true
                }
                return false
            }
        })

        // accelerometer
        txt = view.findViewById(R.id.list_accelerometer) as TextView
        txt.setOnClickListener {
            findNavController().navigate(R.id.action_ListFragment_to_AccelerometerFragment)
        }
        txt.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent): Boolean {
                if (event.action == MotionEvent.ACTION_UP) {
                    if (event.rawX < txt.layout.width - txt.compoundPaddingEnd) {
                        // open fragment
                        findNavController().navigate(R.id.action_ListFragment_to_AccelerometerFragment)
                    } else {
                        // open settings dialog
                        val dialog = AccelerometerDialog()
                        dialog.show(requireActivity().supportFragmentManager, "accelerometerDialog")
                    }
                    return true
                }
                return false
            }
        })


        // orientation
        txt = view.findViewById(R.id.list_orientation) as TextView
        txt.setOnClickListener {
            Log.v(TAG, "onClick orientation")
            findNavController().navigate(R.id.action_ListFragment_to_OrientationFragment)
        }
        txt.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent): Boolean {
                Log.v(TAG, "onTouch orientation")

                if (event.action == MotionEvent.ACTION_UP) {
                    if (event.rawX < txt.layout.width - txt.compoundPaddingEnd) {
                        // open fragment
                        findNavController().navigate(R.id.action_ListFragment_to_OrientationFragment)
                    } else {
                        // open settings dialog
                        val dialog = OrientationDialog()
                        dialog.show(requireActivity().supportFragmentManager, "OrientationDialog")
                    }
                    return true
                }
                return false
            }
        })

        // beacon
        txt = view.findViewById(R.id.list_beacon) as TextView
        txt.setOnClickListener {
            Log.v(TAG, "onClick beacon")
            findNavController().navigate(R.id.action_ListFragment_to_BeaconFragment)
        }

        txt.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent): Boolean {
                if (event.action == MotionEvent.ACTION_UP) {
                    if (event.rawX < txt.layout.width - txt.compoundPaddingEnd) {
                        if (verifyBluetooth(v!!) &&
                            locationPermission(
                                v,
                                REQUEST_BACKGROUND_LOCATION_FOR_BEACON_TO_FRAGMENT
                            )
                        )
                            findNavController().navigate(R.id.action_ListFragment_to_BeaconFragment)
                    } else {
                        // open settings dialog
                        if (verifyBluetooth(v!!) &&
                            locationPermission(
                                v,
                                REQUEST_BACKGROUND_LOCATION_FOR_BEACON_TO_SETTINGS
                            )
                        ) {
                            val dialog = BeaconDialog()
                            dialog.show(requireActivity().supportFragmentManager, "BeaconDialog")
                        }
                    }
                    return true
                }
                return false
            }
        })

        // exponotif
        txt = view.findViewById(R.id.list_exponotif) as TextView
        txt.setOnClickListener {
            // never gets called, but somehow necessary
            Log.v(TAG, "onClick exponotif")
            findNavController().navigate(R.id.action_ListFragment_to_ExponotifFragment)
        }

        txt.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent): Boolean {
                if (event.action == MotionEvent.ACTION_UP) {
                    if (event.rawX < txt.layout.width - txt.compoundPaddingEnd) {
                        if (verifyBluetooth(v!!) &&
                            locationPermission(
                                v,
                                REQUEST_BACKGROUND_LOCATION_FOR_EXPONOTIF_TO_FRAGMENT
                            )
                        )
                            findNavController().navigate(R.id.action_ListFragment_to_ExponotifFragment)
                    } else {
                        // open settings dialog
                        if (verifyBluetooth(v!!) &&
                            locationPermission(
                                v,
                                REQUEST_BACKGROUND_LOCATION_FOR_EXPONOTIF_TO_SETTINGS
                            )
                        ) {
                            val dialog = ExponotifDialog()
                            dialog.show(requireActivity().supportFragmentManager, "ExponotifDialog")
                        }
                    }
                    return true
                }
                return false
            }
        })

        // GPS
        txt = view.findViewById(R.id.list_satnav) as TextView
        txt.setOnClickListener {
            // never gets called, but somehow necessary
            Log.v(TAG, "onClick satnav")
            findNavController().navigate(R.id.action_ListFragment_to_SatnavFragment)
        }

        txt.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent): Boolean {
                if (event.action == MotionEvent.ACTION_UP) {
                    if (event.rawX < txt.layout.width - txt.compoundPaddingEnd) {
                        if (verifyGPS(v!!) &&
                            locationPermission(
                                v,
                                REQUEST_BACKGROUND_LOCATION_FOR_SATNAV_TO_FRAGMENT
                            )
                        )
                            findNavController().navigate(R.id.action_ListFragment_to_SatnavFragment)
                    } else {
                        // open settings dialog
                        if (verifyGPS(v!!) &&
                            locationPermission(
                                v,
                                REQUEST_BACKGROUND_LOCATION_FOR_SATNAV_TO_SETTINGS
                            )
                        ) {
                            val dialog = SatnavDialog()
                            dialog.show(requireActivity().supportFragmentManager, "SatnavDialog")
                        }
                    }
                    return true
                }
                return false
            }
        })

        // setup audio dialog
        txt = view.findViewById(R.id.list_audio) as TextView
        txt.setOnClickListener {
            // verifyAudio permissions?
            findNavController().navigate(R.id.action_ListFragment_to_AudioFragment)
        }
        txt.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent): Boolean {
                if (event.action == MotionEvent.ACTION_UP) {
                    if (event.rawX < txt.layout.width - txt.compoundPaddingEnd) {
                        // open audio fragment
                        if (verifyAudio(v, true))
                            findNavController().navigate(R.id.action_ListFragment_to_AudioFragment)
                    } else {
                        // open settings dialog
                        if (verifyAudio(v, false)) {
                            val dialog = AudioDialog()
                            dialog.show(requireActivity().supportFragmentManager, "AudioDialog")
                        }
                    }
                    return true
                }
                return false
            }
        })


    }

    // preferences have been changed by the user. update bold/un-bold
    override fun onSharedPreferenceChanged(pref: SharedPreferences?, key: String?) {
        updateListItems(pref)
    }

    // bold / un-bold according to the preferences
    private fun updateListItems(sharedPref: SharedPreferences?) {
        if (activity != null && sharedPref != null) {
            updateTypeFace(R.string.light_on_key, LIGHT_ON_DEFAULT, R.id.list_light, sharedPref)
            updateTypeFace(R.string.linacc_on_key, LINACC_ON_DEFAULT, R.id.list_linacc, sharedPref)
            updateTypeFace(
                R.string.accelerometer_on_key,
                ACCELEROMETER_ON_DEFAULT,
                R.id.list_accelerometer,
                sharedPref
            )
            updateTypeFace(
                R.string.orientation_on_key,
                ORIENTATION_ON_DEFAULT,
                R.id.list_orientation,
                sharedPref
            )
            updateTypeFace(R.string.beacon_on_key, BEACON_ON_DEFAULT, R.id.list_beacon, sharedPref)
            updateTypeFace(
                R.string.exponotif_on_key,
                EXPONOTIF_ON_DEFAULT,
                R.id.list_exponotif,
                sharedPref
            )
            updateTypeFace(R.string.satnav_on_key, SATNAV_ON_DEFAULT, R.id.list_satnav, sharedPref)
            updateTypeFace(R.string.audio_on_key, AUDIO_ON_DEFAULT, R.id.list_audio, sharedPref)
        }
    }

    private fun updateTypeFace(
        prefKey: Int,
        default: Boolean,
        viewId: Int,
        sharedPref: SharedPreferences,
    ) {
        if (activity == null) return
        val on = sharedPref.getBoolean(getString(prefKey), default)
        val view = requireActivity().findViewById(viewId) as TextView? ?: return
        view.typeface = if (on) Typeface.DEFAULT_BOLD else Typeface.DEFAULT
    }


    // check if bluetooth is available
    private fun verifyBluetooth(v: View): Boolean {
        val a = activity as MainActivity

        // Checks if Bluetooth is supported on the device.
        if (!a.packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            bleNotSupported(v.context)
            return false
        }

        // Try to initialize a Bluetooth adapter.  For API level 18 and above, get a reference to
        // BluetoothAdapter through BluetoothManager.
        val bluetoothManager =
            v.context.getSystemService(ListActivity.BLUETOOTH_SERVICE) as BluetoothManager

        if (bluetoothManager.adapter == null || !bluetoothManager.adapter.isEnabled) {
            bluetoothNotEnabled(v.context)
            return false
        }
        return true
    }


    // check if GPS is available
    private fun verifyGPS(v: View): Boolean {
        val a = activity as MainActivity

        // Checks if GPS is supported on the device.
        if (!a.packageManager.hasSystemFeature(PackageManager.FEATURE_LOCATION_GPS)) {
            gpsNotSupported(v.context)
            return false
        }

        // Try to initialize GPS
        val manager = a.getSystemService(LOCATION_SERVICE) as LocationManager?
        val enabled = manager == null || manager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        if (!enabled) {
            gpsNotEnabled(v.context)
            return false
        }
        return true
    }



    private fun locationPermission(v: View, requestCode: Int):Boolean {
        // since android 6, permission must be granted at runtime
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (v.context.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED
            ) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    if (v.context.checkSelfPermission(Manifest.permission.ACCESS_BACKGROUND_LOCATION)
                        != PackageManager.PERMISSION_GRANTED
                    ) {
                        if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_BACKGROUND_LOCATION)) {
                            // Should we show an explanation?
                            val builder = AlertDialog.Builder(v.context)
                            builder.setTitle(R.string.background_location_permission)
                            builder.setMessage(R.string.background_location_permission_explain)
                            builder.setPositiveButton(android.R.string.ok, null)
                            builder.setOnDismissListener {
                                requestPermissions(
                                    arrayOf(Manifest.permission.ACCESS_BACKGROUND_LOCATION),
                                    requestCode
                                )
                            }
                            builder.show()
                        } else
                            backgroundLocationNotGranted(v.context)
                        return false
                    }
                } // end if Q
            } else {
                // fine location wasn't granted. ask for permission
                if (!shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    requestPermissions(
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
                            arrayOf(
                                Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_BACKGROUND_LOCATION
                            )
                        else
                            arrayOf(
                                Manifest.permission.ACCESS_FINE_LOCATION
                            ),
                        requestCode
                    )
                } else
                    fineLocationNotGranted(v.context)

                return false
            }
        } // end if > M

        Log.d(TAG, "location permission ok")
        return true
    }

    // dialogs guiding through that mess
    private fun fineLocationNotGranted(c: Context?) {
        showMyDialog(
            c, R.string.fine_location_permission,
            R.string.fine_location_permission_explain
        )
    }

    private fun backgroundLocationNotGranted(c: Context?) {
        showMyDialog(
            c, R.string.background_location_permission,
            R.string.background_location_permission_denial
        )
    }
    private fun bluetoothNotEnabled(c: Context?) {
        showMyDialog(
            c, R.string.bluetooth_not_enabled,
            R.string.enable_bluetooth
        )
    }

    private fun bleNotSupported(c: Context?) {
        showMyDialog(
            c, R.string.bluetooth_le_not_available,
            R.string.bluetooth_le_not_supported
        )
    }

    private fun gpsNotEnabled(c: Context?) {
        showMyDialog(
            c, R.string.GPS_not_enabled,
            R.string.enable_GPS
        )
    }

    private fun gpsNotSupported(c: Context?) {
        showMyDialog(
            c, R.string.GPS_not_available,
            R.string.GPS_not_supported
        )
    }

    private fun showMyDialog(c: Context?, title: Int, message: Int) {
        if (c == null) return
        val builder = AlertDialog.Builder(c)
        builder.setTitle(title)
        builder.setMessage(message)
        builder.setPositiveButton(android.R.string.ok, null)
        builder.setOnDismissListener {
            it.dismiss()
        }
        builder.show()
    }

    fun verifyAudio(v: View?, nextIsFragment: Boolean):Boolean {
        val a = activity as MainActivity
        if (v == null) return false
        if (ContextCompat.checkSelfPermission(a, Manifest.permission.RECORD_AUDIO)
            != PackageManager.PERMISSION_GRANTED) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.RECORD_AUDIO)) {
                    // Should we show an explanation?
                    val builder = AlertDialog.Builder(v.context)
                    builder.setTitle(R.string.recording_permission)
                    builder.setMessage(R.string.recording_permission_explain)
                    builder.setPositiveButton(android.R.string.ok, null)
                    builder.setOnDismissListener {
                        requestPermissions(
                            arrayOf(Manifest.permission.RECORD_AUDIO),
                            if (nextIsFragment)
                                REQUEST_RECORD_AUDIO_TO_FRAGMENT
                            else
                                REQUEST_RECORD_AUDIO_TO_SETTINGS
                        )
                    }
                    builder.show()
                } else
                    requestPermissions(
                        arrayOf(Manifest.permission.RECORD_AUDIO),
                        if (nextIsFragment)
                            REQUEST_RECORD_AUDIO_TO_FRAGMENT
                        else
                            REQUEST_RECORD_AUDIO_TO_SETTINGS
                    )
            return false
        }
        Log.d(TAG, "verify audio ok")
        return true
    }

    // called with the result of asking permissions
    // we only handle it in order to open the fragment or dialog
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        Log.d(TAG, "onRequestPermissionsResult $requestCode $permissions")
        if (requestCode == REQUEST_FINE_LOCATION_FOR_BEACON_TO_FRAGMENT ||
            requestCode == REQUEST_FINE_LOCATION_FOR_BEACON_TO_SETTINGS                     
        ) {
            if (grantResults.all { it == PackageManager.PERMISSION_GRANTED }) {
                // open either the fragment of the settings dialog
                // misusing the request code to carry status information around
                if (requestCode == REQUEST_FINE_LOCATION_FOR_BEACON_TO_FRAGMENT)
                    findNavController().navigate(R.id.action_ListFragment_to_BeaconFragment)
                else if (requestCode == REQUEST_FINE_LOCATION_FOR_BEACON_TO_SETTINGS) {
                    val dialog = BeaconDialog()
                    dialog.show(requireActivity().supportFragmentManager, "BeaconDialog")
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
                    backgroundLocationNotGranted(context)
                else
                    fineLocationNotGranted(context)
            }
        } else if (requestCode == REQUEST_FINE_LOCATION_FOR_EXPONOTIF_TO_FRAGMENT ||
            requestCode == REQUEST_FINE_LOCATION_FOR_EXPONOTIF_TO_SETTINGS
        ) {
            if (grantResults.all { it == PackageManager.PERMISSION_GRANTED }) {
                // open either the fragment of the settings dialog
                // misusing the request code to carry status information around
                if (requestCode == REQUEST_FINE_LOCATION_FOR_EXPONOTIF_TO_FRAGMENT)
                    findNavController().navigate(R.id.action_ListFragment_to_ExponotifFragment)
                else if (requestCode == REQUEST_FINE_LOCATION_FOR_EXPONOTIF_TO_SETTINGS) {
                    val dialog = ExponotifDialog()
                    dialog.show(requireActivity().supportFragmentManager, "ExponotifDialog")
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
                    backgroundLocationNotGranted(context)
                else
                    fineLocationNotGranted(context)
            }
        } else if (requestCode == REQUEST_FINE_LOCATION_FOR_SATNAV_TO_FRAGMENT ||
            requestCode == REQUEST_FINE_LOCATION_FOR_SATNAV_TO_SETTINGS
        ) {
            if (grantResults.all { it == PackageManager.PERMISSION_GRANTED }) {
                // open either the fragment of the settings dialog
                // misusing the request code to carry status information around
                if (requestCode == REQUEST_FINE_LOCATION_FOR_SATNAV_TO_FRAGMENT)
                    findNavController().navigate(R.id.action_ListFragment_to_SatnavFragment)
                else if (requestCode == REQUEST_FINE_LOCATION_FOR_SATNAV_TO_SETTINGS) {
                    val dialog = SatnavDialog()
                    dialog.show(requireActivity().supportFragmentManager, "SatnavDialog")
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
                    backgroundLocationNotGranted(context)
                else
                    fineLocationNotGranted(context)
            }

        } else if (requestCode == REQUEST_RECORD_AUDIO_TO_FRAGMENT ||
            requestCode == REQUEST_RECORD_AUDIO_TO_SETTINGS
        ) {
            if (grantResults.all { it == PackageManager.PERMISSION_GRANTED }) {
                // open either the fragment of the settings dialog
                // misusing the request code to carry status information around
                if (requestCode == REQUEST_RECORD_AUDIO_TO_FRAGMENT)
                    findNavController().navigate(R.id.action_ListFragment_to_AudioFragment)
                else { // settings then
                    val dialog = AudioDialog()
                    dialog.show(requireActivity().supportFragmentManager, "AudioDialog")
                }
            } else {
                recordingNotGranted(context)
            }

        }
    }

    private fun recordingNotGranted(c: Context?) {
        showMyDialog(
            c, R.string.recording_permission,
            R.string.recording_permission_denial
        )
    }

}

