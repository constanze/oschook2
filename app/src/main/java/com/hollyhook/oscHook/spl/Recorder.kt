/*********************************************
 * ANDROID SOUND PRESSURE METER APPLICATION
 * DESC   : Recording Thread that calculates SPL.
 * WEBSRC : Recording : http://www.anddev.org/viewtopic.php?p=22820
 * AUTHOR : hashir.mail@gmail.com
 * DATE   : 19 JUNE 2009
 * CHANGES: - Changed the recording logic
 * - Added logic to pass recorded buffer to FFT
 * - Added logic to calculate SPL.
 */
package com.hollyhook.oscHook.spl

import android.media.AudioFormat
import android.media.AudioRecord
import android.media.MediaRecorder
import android.os.Bundle
import android.os.Handler
import android.os.Process
import android.util.Log
import com.hollyhook.oscHook.OscHookService.Companion.MSG_RECORDING
import com.hollyhook.oscHook.OscHookService.Companion.MSG_RECORDING_FAIL
import com.hollyhook.oscHook.spl.FFT.fft
import java.math.BigDecimal
import kotlin.math.log10
import kotlin.math.sqrt

class Recorder(h: Handler) : Runnable {
    private val TAG = "Recorder"
    /**
     * the channelConfiguration to set
     */
    private var channelConfiguration = 0

    /**
     * the frequency to set
     */
    private var sampleRate = 0
    private var handle: Handler
    private var bufferSizeChanged = false

    // change buffer size, check 2^n
    // has no effect if changed while the tread is running
    var bufferSize: Short = 512 // 1024 // 4096 // 2048
        set (b) {
            var newVal:Short = 512
            var ok = false
            var two: Short = 1
            for (n: Int in 0..15) {
                if (two == b) {
                    newVal = b
                    Log.v(TAG, "fft buffer size $b")
                    ok = true
                    break
                }
                two = (two * 2).toShort()
            }
            if (!ok) {
                Log.w(TAG, "invalid buffer size, must be 2^n. Using default 512")
            }

            // a change in buffer size requested while thread is running
            bufferSizeChanged = (newVal != bufferSize) && isRecording
            field = newVal
        }

    @Volatile
    var isRecording = false
    private lateinit var tempBuffer: ShortArray

    /**
     * @return the audioEncoding
     */
    private val audioEncoding: Int
        get() = Companion.audioEncoding


    /**
     * Calculate SPL P = square root ( 2*Z*I ) - > Pressure Z = Acoustic
     * Impedance = 406.2 for air at 30 degree celsius I = Intensity = 2*Z*pi
     * square*frequency square*Amplitude square
     *
     * @param bsize
     * - the size of FFT required.
     */
    private fun measure(bsize: Int): Double {
        var i = 0
        var max = 0.0
        var maxIndex = 0
        var w: Double
        val z = 406.2
        val p0 = 2 * 0.00001 // is constant
        var iStar = 0.0 // SPL
        val x = arrayOfNulls<Complex>(bsize)

        while (i < bsize) {
            x[i] = Complex(tempBuffer[i].toDouble(), 0.0)
            i++
        }
        val xf: Array<Complex?> = fft(x) // <---
        i = 1
        while (i < bsize / 2) {
            w = xf[i]!!.abs()
            if (w > max) {
                maxIndex = i
                max = w
            }
            i++
        }
        // Frequency and Amp of fundamental frequency
        val frequency = maxIndex * bsize.toDouble()
        val amplitude = max * 2 / bsize
        val ii = (z * 2 * 2 * Math.PI * Math.PI * frequency * frequency * amplitude
                * amplitude)
        val p = sqrt(z * ii)
        if (p != 0.0) iStar = round(20 * log10(p / p0) / 10, 3) // divide by 10 to
        // correct the calculation

        val b = Bundle()
        b.putFloat("frequency", frequency.toFloat()*8000 /(512*bsize))
        b.putFloat("spl", iStar.toFloat())

        val msg = handle.obtainMessage(MSG_RECORDING)
        msg.data = b
        handle.sendMessage(msg)
        return iStar
    }

    /**
     * Utility Function to round a double value
     *
     * @param d
     * - The decimal value
     * @param decimalPlace
     * - how many places required
     * @return double - the rounded value
     */
    @Suppress("SameParameterValue")
    private fun round(d: Double, decimalPlace: Int): Double {
        // see the Javadoc about why we use a String in the constructor
        // http://java.sun.com/j2se/1.5.0/docs/api/java/math/BigDecimal.html#BigDecimal(double)
        var bd = BigDecimal(d.toString())
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP)
        return bd.toDouble()
    }



    /* Recording THREAD */
    override fun run() {
        var recordInstance: AudioRecord?

        // We're important...
        Process.setThreadPriority(Process.THREAD_PRIORITY_URGENT_AUDIO)

        recordInstance = AudioRecord(
            MediaRecorder.AudioSource.MIC, sampleRate, channelConfiguration, this
                .audioEncoding, bufferSize.toInt()
        )
        if (recordInstance.state != AudioRecord.STATE_INITIALIZED) {
            // didn't work and don't know how to fix
            val msg = handle.obtainMessage(MSG_RECORDING_FAIL)
            handle.sendMessage(msg)
            return
        }

        tempBuffer = ShortArray(bufferSize.toInt())
        recordInstance.startRecording()
        // Continue till flag is changed by the service
        while (isRecording) {
            for (i in tempBuffer.indices) {
                tempBuffer[i] = 0
            }
            recordInstance!!.read(tempBuffer, 0, tempBuffer.size)
            measure(tempBuffer.size) // calculate SPL
            // could be changed any time by the settings
            if (bufferSizeChanged)  {
                recordInstance.stop()
                tempBuffer = ShortArray(bufferSize.toInt()) // new buffer size
                recordInstance = AudioRecord(
                    MediaRecorder.AudioSource.MIC, sampleRate, channelConfiguration, this
                        .audioEncoding, bufferSize.toInt()
                )
                recordInstance.startRecording()
                bufferSizeChanged = false
            }

        }
        // Close recording
        recordInstance?.stop()
    }

    companion object {
        private const val audioEncoding = AudioFormat.ENCODING_PCM_16BIT
    }

    /**
     * Handler is passed to pass messages to main screen Recording is done
     * 8000Hz MONO 16 bit
     */
    init {
        sampleRate = 8000 // resolution 8000/512 = 15.625 Hz per bin
        channelConfiguration = AudioFormat.CHANNEL_IN_MONO
        handle = h
    }
}