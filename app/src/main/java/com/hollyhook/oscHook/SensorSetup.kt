package com.hollyhook.oscHook

import android.content.Context
import android.content.SharedPreferences
import android.content.res.Resources
import android.hardware.Sensor
import android.os.Bundle
import android.os.Message
import android.os.Messenger
import android.os.RemoteException
import android.util.Log

// used by the service
sealed class SensorSetup(val context: Context,
                         val dimension: Int,
                         val onKey: Int,
                         private val onDefault: Boolean,
                         private val slideKey: Int,
                         private val slideDefault: Int,
                         private val inMinKey: Int,
                         private val inMinDefault: Float,
                         private val inMaxKey: Int,
                         private val inMaxDefault: Float,
                         private val outMinKey: Int,
                         private val outMinDefault: Float,
                         private val outMaxKey: Int,
                         private val outMaxDefault: Float,
                         val registerMsg: Int,
                         val unregisterMsg: Int,
                         private val clientMsg: Int
) {
    // setup for light sensor
    class Light(c: Context): SensorSetup(
        c,
        1,
        R.string.light_on_key, LIGHT_ON_DEFAULT, // the only one which is on per default
        R.string.light_smoothing_key, LIGHT_SMOOTHING_DEFAULT,
        R.string.light_raw_min_key, LIGHT_RAW_MIN_DEFAULT,
        R.string.light_raw_max_key, LIGHT_RAW_MAX_DEFAULT,
        R.string.light_out_min_key, LIGHT_OUT_MIN_DEFAULT,
        R.string.light_out_max_key, LIGHT_OUT_MAX_DEFAULT,
        OscHookService.MSG_REGISTER_LIGHT_CLIENT,
        OscHookService.MSG_UNREGISTER_LIGHT_CLIENT,
        OscHookService.MSG_LIGHT
    ) {
        override val name = "light"
        override fun restoreSharedPreferences(sharedPref: SharedPreferences) {
            setOscAddressKeysAndDefaults(intArrayOf(R.string.light_osc_key), arrayOf(LIGHT_OSC_DEFAULT))
            readPreferences(sharedPref)
        }
    }

    // linear acceleration
    class Linacc(c: Context): SensorSetup(
        c,
        3,
        R.string.linacc_on_key, LINACC_ON_DEFAULT,
        R.string.linacc_smoothing_key, LINACC_SMOOTHING_DEFAULT,
        R.string.linacc_raw_min_key, LINACC_RAW_MIN_DEFAULT,
        R.string.linacc_raw_max_key, LINACC_RAW_MAX_DEFAULT,
        R.string.linacc_out_min_key, LINACC_OUT_MIN_DEFAULT,
        R.string.linacc_out_max_key, LINACC_OUT_MAX_DEFAULT,
        OscHookService.MSG_REGISTER_LINACC_CLIENT,
        OscHookService.MSG_UNREGISTER_LINACC_CLIENT,
        OscHookService.MSG_LINACC
    ) {
        override val name = "linear acceleration"
        override fun restoreSharedPreferences(sharedPref: SharedPreferences) {
            setOscAddressKeysAndDefaults(
                intArrayOf(
                    R.string.linacc_x_osc_key,
                    R.string.linacc_y_osc_key,
                    R.string.linacc_z_osc_key
                ),
                arrayOf(
                    LINACC_X_OSC_DEFAULT,
                    LINACC_Y_OSC_DEFAULT,
                    LINACC_Z_OSC_DEFAULT
                )
            )
            readPreferences(sharedPref)
        }

    }

    // accelerometer
    class Accelerometer(c: Context): SensorSetup(
        c,
        3,
        R.string.accelerometer_on_key, ACCELEROMETER_ON_DEFAULT,
        R.string.accelerometer_smoothing_key, ACCELEROMETER_SMOOTHING_DEFAULT,
        R.string.accelerometer_raw_min_key, ACCELEROMETER_RAW_MIN_DEFAULT,
        R.string.accelerometer_raw_max_key, ACCELEROMETER_RAW_MAX_DEFAULT,
        R.string.accelerometer_out_min_key, ACCELEROMETER_OUT_MIN_DEFAULT,
        R.string.accelerometer_out_max_key, ACCELEROMETER_OUT_MAX_DEFAULT,
        OscHookService.MSG_REGISTER_ACCELEROMETER_CLIENT,
        OscHookService.MSG_UNREGISTER_ACCELEROMETER_CLIENT,
        OscHookService.MSG_ACCELEROMETER
    ) {
        override val name = "accelerometer"
        override fun restoreSharedPreferences(sharedPref: SharedPreferences) {
            setOscAddressKeysAndDefaults(
                intArrayOf(
                    R.string.accelerometer_x_osc_key,
                    R.string.accelerometer_y_osc_key,
                    R.string.accelerometer_z_osc_key
                ),
                arrayOf(
                    ACCELEROMETER_X_OSC_DEFAULT,
                    ACCELEROMETER_Y_OSC_DEFAULT,
                    ACCELEROMETER_Z_OSC_DEFAULT
                )
            )
            readPreferences(sharedPref)
        }

    }

    // setup for orientation sensor
    class Orientation(c: Context): SensorSetup(
        c,
        3,
        R.string.orientation_on_key, ORIENTATION_ON_DEFAULT,
        R.string.orientation_smoothing_key, ORIENTATION_SMOOTHING_DEFAULT, // smooth a bit
        R.string.orientation_raw_min_key, ORIENTATION_RAW_MIN_DEFAULT,
        R.string.orientation_raw_max_key, ORIENTATION_RAW_MAX_DEFAULT,
        R.string.orientation_out_min_key, ORIENTATION_OUT_MIN_DEFAULT,
        R.string.orientation_out_max_key, ORIENTATION_OUT_MAX_DEFAULT,
        OscHookService.MSG_REGISTER_ORIENTATION_CLIENT,
        OscHookService.MSG_UNREGISTER_ORIENTATION_CLIENT,
        OscHookService.MSG_ORIENTATION
    ) {
        override val name = "orientation"
        override fun restoreSharedPreferences(sharedPref: SharedPreferences) {
            setOscAddressKeysAndDefaults(
                intArrayOf(
                    R.string.orientation_alpha_osc_key,
                    R.string.orientation_beta_osc_key,
                    R.string.orientation_gamma_osc_key
                ),
                arrayOf(
                    ORIENTATION_ALPHA_OSC_DEFAULT,
                    ORIENTATION_BETA_OSC_DEFAULT,
                    ORIENTATION_GAMMA_OSC_DEFAULT
                )
            )
            readPreferences(sharedPref)
        }
    }

    class Beacon(c: Context): SensorSetup(
        c,
        MAX_BEACONS,
        R.string.beacon_on_key, BEACON_ON_DEFAULT,
        0, 0, // not used
        R.string.beacon_raw_min_key, BEACON_RAW_MIN_DEFAULT,
        R.string.beacon_raw_max_key, BEACON_RAW_MAX_DEFAULT,
        R.string.beacon_out_min_key, BEACON_OUT_MIN_DEFAULT,
        R.string.beacon_out_max_key, BEACON_OUT_MAX_DEFAULT,
        OscHookService.MSG_REGISTER_BEACON_CLIENT,
        OscHookService.MSG_UNREGISTER_BEACON_CLIENT,
        OscHookService.MSG_BEACON
    ) {
        override val name = "beacon"
        val beaconList = Array<AgingBeacon?>(MAX_BEACONS) { null }

        override fun process() {
           processBLE(beaconList)
        }
        override fun postProcess(trackingTime: Int) {
            postProcessBLE(DEFAULT_TRACKING_TIME, beaconList)
        }
        override fun sendMessageToClient() {
            sendToClientBLE(beaconList)
        }
        override fun getOscAddress(i: Int): String {
            val oscAddress = oscAddresses[i]
            // replace placeholder for the mac address of the beacon
            return oscAddress.replace("%a", beaconList[i]?.address ?: "")
        }
        override fun restoreSharedPreferences(sharedPref: SharedPreferences) {
            setOscAddressKeysAndDefaults(
                intArrayOf(
                    R.string.beacon_1_osc_key,
                    R.string.beacon_2_osc_key,
                    R.string.beacon_3_osc_key,
                    R.string.beacon_4_osc_key
                ),
                arrayOf(
                    BEACON_1_OSC_DEFAULT,
                    BEACON_2_OSC_DEFAULT,
                    BEACON_3_OSC_DEFAULT,
                    BEACON_4_OSC_DEFAULT,
                )
            )
            readPreferences(sharedPref)
        }
    }

    class Exponotif(c: Context): SensorSetup(
        c,
        MAX_EXPONOTIFS,
        R.string.exponotif_on_key, EXPONOTIF_ON_DEFAULT,
        0, 0, // not used
        R.string.exponotif_raw_min_key, EXPONOTIF_RAW_MIN_DEFAULT,
        R.string.exponotif_raw_max_key, EXPONOTIF_RAW_MAX_DEFAULT,
        R.string.exponotif_out_min_key, EXPONOTIF_OUT_MIN_DEFAULT,
        R.string.exponotif_out_max_key, EXPONOTIF_OUT_MAX_DEFAULT,
        OscHookService.MSG_REGISTER_EXPONOTIF_CLIENT,
        OscHookService.MSG_UNREGISTER_EXPONOTIF_CLIENT,
        OscHookService.MSG_EXPONOTIF
    ) {
        override val name = "exposure notification"
        val exponotifList = Array<AgingBeacon?>(MAX_EXPONOTIFS) { null }

        override fun process() {
            processBLE(exponotifList)
        }
        override fun postProcess(trackingTime: Int) {
            postProcessBLE(trackingTime, exponotifList)
        }

        override fun sendMessageToClient() {
            sendToClientBLE(exponotifList)
        }
        override fun getOscAddress(i: Int): String {
            val oscAddress = oscAddresses[i]
            return oscAddress.replace("%a", exponotifList[i]?.address ?: "")
        }

        override fun restoreSharedPreferences(sharedPref: SharedPreferences) {
            // mysterious crash in v26 with a NotFoundException,
            // therefore try to secure with an try/catch
            val ks: String = try {
                context.getString(R.string.exponotif_osc_key)
            } catch (e: Resources.NotFoundException) {
                // and what are we going to do now?
                "exponotif_osc_key"
            }
            for (i in oscAddressKeyStrings.indices) {
                // all are identical
                oscAddressKeys[i] = R.string.exponotif_osc_key
                oscAddressDefaults[i] = EXPONOTIF_OSC_DEFAULT
                oscAddressKeyStrings[i] = "$ks.$i"
            }
            readPreferences(sharedPref)
        }
    }

    // setup for satnav sensor
    class Satnav(c: Context): SensorSetup(
        c,
        2, // either longitude / latitude, or distance / bearing
        R.string.satnav_on_key, SATNAV_ON_DEFAULT,
        0, 0, // smooth not
        0, 0f, // not used
        0, 0f,
        0, 0f,
        0, 0f,
        OscHookService.MSG_REGISTER_SATNAV_CLIENT,
        OscHookService.MSG_UNREGISTER_SATNAV_CLIENT,
        OscHookService.MSG_SATNAV
    ) {
        override val name = "gps"
        var useOffsets: Boolean = true

        // called from timer tick
        override fun process() {
            // no change in values, scaling makes no sense
            for (i in measuredValues.indices) {
                outValues[i] = measuredValues[i]
            }
        }
        override fun postProcess(trackingTime: Int) {
            for (i in outValues.indices) {
                outValues[i] = Float.NaN // don't send it again
                measuredValues[i] = Float.NaN
            }
        }

        override fun restoreSharedPreferences(sharedPref: SharedPreferences) {
            val spinnerSelection = sharedPref.getInt(
                context.getString(R.string.satnav_spinner_key),
                SATNAV_SPINNER_DEFAULT
            )
            useOffsets = SATNAV_SPINNER_DEFAULT == spinnerSelection

            if (!useOffsets) {
                setOscAddressKeysAndDefaults(
                    intArrayOf(
                        R.string.satnav_longitude_osc_key,
                        R.string.satnav_latitude_osc_key
                    ),
                    arrayOf(
                        SATNAV_LONGITUDE_OSC_DEFAULT,
                        SATNAV_LATITUDE_OSC_DEFAULT
                    )
                )
            } else {
                setOscAddressKeysAndDefaults(
                    intArrayOf(
                        R.string.satnav_distance_osc_key,
                        R.string.satnav_bearing_osc_key
                    ),
                    arrayOf(
                        SATNAV_DISTANCE_OSC_DEFAULT,
                        SATNAV_BEARING_OSC_DEFAULT
                    )
                )
            }
            readPreferences(sharedPref)
        }
    }

    // setup for audio sensor
    class Audio(c: Context): SensorSetup(
        c,
        2,
        R.string.audio_on_key, AUDIO_ON_DEFAULT,
        R.string.audio_smoothing_key, AUDIO_SMOOTHING_DEFAULT,
        R.string.audio_spl_raw_min_key, AUDIO_RAW_MIN_DEFAULT,
        R.string.audio_spl_raw_max_key, AUDIO_RAW_MAX_DEFAULT,
        R.string.audio_spl_out_min_key, AUDIO_OUT_MIN_DEFAULT,
        R.string.audio_spl_out_max_key, AUDIO_OUT_MAX_DEFAULT,
        OscHookService.MSG_REGISTER_AUDIO_CLIENT,
        OscHookService.MSG_UNREGISTER_AUDIO_CLIENT,
        OscHookService.MSG_AUDIO
    ) {
        override val name = "audio"
        override fun process() {
            val divisor = divisor(inMin, inMax)
            val newAudio =
                outMin + (outMax - outMin) * (measuredValues[0] - inMin) / divisor
            // smooth, same formula as in max
            // y (n) = y (n-1) + ((x (n) - y (n-1))/slide)
            if (slide > 1 && !outValues[0].isNaN())
                outValues[0] += (newAudio.toFloat() - outValues[0]) / slide.toFloat()
            else
                outValues[0] = newAudio.toFloat()
            outValues[1] = measuredValues[1] // no scaling for frequency
        }

        override fun restoreSharedPreferences(sharedPref: SharedPreferences) {
            setOscAddressKeysAndDefaults(
                intArrayOf(R.string.audio_osc_spl_key, R.string.audio_osc_frequency_key),
                arrayOf(AUDIO_OSC_SPL_DEFAULT, AUDIO_OSC_FREQUENCY_DEFAULT)
            )
            readPreferences(sharedPref)
        }
    }


    val TAG = "SensorSetup"
    var client: Messenger? = null
    var sensor: Sensor? = null

    var on = onDefault
    var slide = slideDefault
    var inMin = inMinDefault
    var inMax = inMaxDefault
    var outMin = outMinDefault
    var outMax = outMaxDefault

    val outValues = FloatArray(dimension)      // scaled
    val measuredValues = FloatArray(dimension) // unscaled
    val oscAddresses: Array<String> = Array(dimension) { "" }
    val oscAddressKeys = IntArray(dimension)
    val oscAddressDefaults: Array<String> = Array(dimension) { "" }
    val oscAddressKeyStrings: Array<String> = Array(dimension) { "" }

    init {
        for (i in outValues.indices)
            outValues[i] = Float.NaN
        for (i in measuredValues.indices)
            measuredValues[i] = Float.NaN
    }

    open fun process() {
        // scaling, but prevent #DIV0
        val divisor = divisor(inMin, inMax)

        for (i in measuredValues.indices) {
            val newVal =
                outMin + (outMax - outMin) * (measuredValues[i] - inMin) / divisor
            // smooth, same formula as in max
            // y (n) = y (n-1) + ((x (n) - y (n-1))/slide)
            if (slide > 1 && !outValues[i].isNaN())
                outValues[i] += (newVal.toFloat() - outValues[i]) / slide.toFloat()
            else
                outValues[i] = newVal.toFloat()
        }
    }

    fun setOscAddressKeysAndDefaults(ka: IntArray, da: Array<String>) {
        if (ka.size != dimension)
            Log.e(TAG, "error dimension")
        else {
            for (i in 0 until dimension) {
                oscAddressKeys[i] = ka[i]
                oscAddressDefaults[i] = da[i]
                oscAddressKeyStrings[i] = context.getString(oscAddressKeys[i])
            }
        }
    }

    open fun getOscAddress(i: Int): String {
        return oscAddresses[i]
    }

    // send the float array to the client
    open fun sendMessageToClient() {
        if (client == null) return
        try {
            // Send data as a Float
            val b = Bundle()
            for (i in 0 until dimension) {
                b.putFloat(oscAddressKeyStrings[i], outValues[i])
            }
            val msg = Message.obtain(null, clientMsg)
            msg.data = b
            client!!.send(msg)
        } catch (e: RemoteException) {
            // The client is dead.
            client = null
        }
    }

    // send the float array to the client
    fun sendEmptyMessageToClient() {
        if (client == null) return
        try {
            // Send data as a Float
            val msg = Message.obtain(null, clientMsg)
            client!!.send(msg)
        } catch (e: RemoteException) {
            // The client is dead.
            client = null
        }
    }

    fun readPreferences(sharedPref: SharedPreferences?) {
        if (sharedPref == null) return
        on = sharedPref.getBoolean(context.getString(onKey), onDefault)
        slide = if (slideKey != 0)
            sharedPref.getInt(context.getString(slideKey), slideDefault)
        else 0

        inMin = if (inMinKey == 0) 0f else sharedPref.getFloat(context.getString(inMinKey), inMinDefault)
        inMax = if (inMaxKey == 0) 0f else sharedPref.getFloat(context.getString(inMaxKey), inMaxDefault)
        outMin = if (outMinKey == 0) 0f else sharedPref.getFloat(context.getString(outMinKey), outMinDefault)
        outMax =  if (outMaxKey == 0) 0f else sharedPref.getFloat(context.getString(outMaxKey), outMaxDefault)
        // load osc addresses
        for (i:Int in oscAddressKeys.indices)
            oscAddresses[i] = sharedPref.getString(
                context.getString(oscAddressKeys[i]),
                oscAddressDefaults[i]
            )!!
    }


    // used in scaling, takes care of #DIV0
    fun divisor(inMin: Float, inMax: Float): Double {
        val eps = 0.00001
        var divisor = (inMax - inMin).toDouble()
        if (divisor > 0 && divisor < eps)
            divisor = eps
        if (divisor < 0 && divisor > -eps)
            divisor = -eps
        return divisor
    }


    // functions for beacons and exposure notifications -----------------

    fun postProcessBLE(
        time: Int,
        myList: Array<AgingBeacon?>,
    ) {
        // set the distance to NaN, to mark the measurements as stale
        // but we've seen them short time ago, hence keep them in myList
        val now: Long = System.currentTimeMillis()
        for (bl in myList) {
            if (bl == null) continue
            bl.distance = Float.NaN
            // look if we can clean up a bit

            if (bl.timestamp > 0 && bl.timestamp < now - time) {
                val pos = myList.indexOf(bl)
                myList[pos] = null
                Log.d(TAG, "$name $pos given up")
            }
        }
    }

    fun sendToClientBLE(
        myList: Array<AgingBeacon?>
    ) {
        if (client != null) {
            try {
                // Send data as a Float and send addresses
                val b = Bundle()
                for (i in 0 until dimension) {
                    b.putFloat(oscAddressKeyStrings[i], outValues[i])
                    b.putString(
                        oscAddressKeyStrings[i] + ".name",
                        myList[i]?.address ?: ""
                    )
                }
                val msg = Message.obtain(null, this.clientMsg)
                msg.data = b // can be full off nans
                client!!.send(msg)
            } catch (e: RemoteException) {
                // The client is dead.
                client = null
            }
        }
    }

    fun processBLE(
        myList: Array<AgingBeacon?>
    ) {
        // scaling, but prevent #DIV0
        val divisor = divisor(inMin, inMax)
        for (i in 0 until dimension) {
            if (myList[i] != null && !myList[i]?.distance?.isNaN()!!) {
                val newVal: Double = outMin.toDouble() +
                        (outMax - outMin) * (myList[i]!!.distance - inMin) / divisor
                outValues[i] = newVal.toFloat()
            } else {
                outValues[i] = Float.NaN
            }
        }
    }

    // search a beacon, and refresh its timestamp
    fun findBeaconInList(
        myBeacon: org.altbeacon.beacon.Beacon,
        list: Array<AgingBeacon?>,
        timestamp: Long
    ): Boolean {
        val address = myBeacon.bluetoothAddress
        var foundInList = false

        for (ab in list) {
            if (ab == null) continue
            val pos = list.indexOf(ab)
            if (ab.address == address) {
                // indeed an already known beacon
                foundInList = true
                ab.timestamp = timestamp
                ab.distance = myBeacon.distance.toFloat()
                // send to client in next timer tick
                Log.v(
                    TAG, String.format(
                        "%s %d in %.01fm (0x%x)", name,
                        pos, myBeacon.distance, myBeacon.serviceUuid
                    )
                )
            }
        }
        return foundInList
    }

    abstract val name: String
    open fun postProcess(trackingTime: Int) {}
    abstract fun restoreSharedPreferences(sharedPref: SharedPreferences)

}