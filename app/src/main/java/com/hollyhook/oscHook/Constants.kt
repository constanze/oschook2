@file:JvmName("Constants")
package com.hollyhook.oscHook


/** how many data points to hold in a series */
const val DATA_LEN = 800 // for 30 sec that would be enough if 16fps

const val COLOR_PLOT3 = 0xFFd97736 // orange
const val COLOR_PLOT4 = 0xFF8c466a // violet
// five is defined in the styles
const val COLOR_PLOT6 = 0xFFd198f2 // pink
const val COLOR_PLOT7 = 0xFF1d9853 // green
const val COLOR_PLOT8 = 0xFFd2c028 // yellow
const val COLOR_PLOT9 = 0xFF34509b // blue
const val COLOR_PLOT10 = 0xFFFFFFFF



const val LEGEND_TEXT_SIZE = 18f
const val PLOT_SHAPE_SIZE = 7f


const val ALTBEACON = "m:2-3=beac,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25"
const val ALTBEACON2 = "m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25"
const val EDDYSTONE_TLM = "x,s:0-1=feaa,m:2-2=20,d:3-3,d:4-5,d:6-7,d:8-11,d:12-15"
const val EDDYSTONE_UID = "s:0-1=feaa,m:2-2=00,p:3-3:-41,i:4-13,i:14-19"
const val EDDYSTONE_URL = "s:0-1=feaa,m:2-2=10,p:3-3:-41,i:4-20v"
const val IBEACON = "m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24"
const val MAX_BEACONS = 4
const val DEFAULT_TRACKING_TIME = 60*1000 // align with R.string.tracking_time_default

// http://www.davidgyoungtech.com/2020/04/24/hacking-with-contact-tracing-beacons
const val EXPOSURE = "s:0-1=fd6f,p:-:-59,i:2-17,d:18-21"
const val ExposureNotificationServiceUuid = 0xFD6F
const val MAX_EXPONOTIFS = 10

// default values
const val LIGHT_ON_DEFAULT = true
const val LIGHT_OSC_DEFAULT = "/light"
const val LIGHT_RAW_MIN_DEFAULT = 0f
const val LIGHT_RAW_MAX_DEFAULT = 1000f
const val LIGHT_OUT_MIN_DEFAULT = 0f
const val LIGHT_OUT_MAX_DEFAULT = 1000f
const val LIGHT_SMOOTHING_DEFAULT = 5

const val LINACC_ON_DEFAULT = false
const val LINACC_X_OSC_DEFAULT = "/linear_acceleration/x"
const val LINACC_Y_OSC_DEFAULT = "/linear_acceleration/y"
const val LINACC_Z_OSC_DEFAULT = "/linear_acceleration/z"
const val LINACC_RAW_MIN_DEFAULT = 0f
const val LINACC_RAW_MAX_DEFAULT = 10f
const val LINACC_OUT_MIN_DEFAULT = 0f
const val LINACC_OUT_MAX_DEFAULT = 1f
const val LINACC_SMOOTHING_DEFAULT = 1 // we want it fast

const val ACCELEROMETER_ON_DEFAULT = false
const val ACCELEROMETER_X_OSC_DEFAULT = "/accelerometer/x"
const val ACCELEROMETER_Y_OSC_DEFAULT = "/accelerometer/y"
const val ACCELEROMETER_Z_OSC_DEFAULT = "/accelerometer/z"
const val ACCELEROMETER_RAW_MIN_DEFAULT = 0f
const val ACCELEROMETER_RAW_MAX_DEFAULT = 10f
const val ACCELEROMETER_OUT_MIN_DEFAULT = 0f
const val ACCELEROMETER_OUT_MAX_DEFAULT = 1f
const val ACCELEROMETER_SMOOTHING_DEFAULT = 1 // we want it fast

const val ORIENTATION_ON_DEFAULT = false
const val ORIENTATION_ALPHA_OSC_DEFAULT = "/orientation/alpha"
const val ORIENTATION_BETA_OSC_DEFAULT = "/orientation/beta"
const val ORIENTATION_GAMMA_OSC_DEFAULT = "/orientation/gamma"
const val ORIENTATION_RAW_MIN_DEFAULT = -180f
const val ORIENTATION_RAW_MAX_DEFAULT = 180f
const val ORIENTATION_OUT_MIN_DEFAULT = -180f
const val ORIENTATION_OUT_MAX_DEFAULT = 180f
const val ORIENTATION_SMOOTHING_DEFAULT = 5

const val BEACON_ON_DEFAULT = false // must be false due to permissions
const val BEACON_1_OSC_DEFAULT = "/beacon/1"
const val BEACON_2_OSC_DEFAULT = "/beacon/2"
const val BEACON_3_OSC_DEFAULT = "/beacon/3"
const val BEACON_4_OSC_DEFAULT = "/beacon/4"
const val BEACON_RAW_MIN_DEFAULT = 0f
const val BEACON_RAW_MAX_DEFAULT = 30f
const val BEACON_OUT_MIN_DEFAULT = 0f
const val BEACON_OUT_MAX_DEFAULT = 30f

const val EXPONOTIF_ON_DEFAULT = false // must be false due to permissions
const val EXPONOTIF_OSC_DEFAULT = "/exponotif/%a"
const val EXPONOTIF_RAW_MIN_DEFAULT = 0f
const val EXPONOTIF_RAW_MAX_DEFAULT = 15f
const val EXPONOTIF_OUT_MIN_DEFAULT = 0f
const val EXPONOTIF_OUT_MAX_DEFAULT = 15f
const val EXPONOTIF_TRACKING_TIME_DEFAULT = 2 // index in the drop down menu

const val SATNAV_ON_DEFAULT = false
// position in spinner 1: GPS, 0: use offsets
// will have side effects if changed to GPS
const val SATNAV_SPINNER_DEFAULT = 0
const val SATNAV_LONGITUDE_OSC_DEFAULT = "/gps/longitude"
const val SATNAV_LATITUDE_OSC_DEFAULT = "/gps/latitude"
const val SATNAV_DISTANCE_OSC_DEFAULT = "/gps/relative/distance"
const val SATNAV_BEARING_OSC_DEFAULT = "/gps/relative/bearing"

const val AUDIO_ON_DEFAULT = false
const val AUDIO_OSC_SPL_DEFAULT = "/audio/spl"
const val AUDIO_OSC_FREQUENCY_DEFAULT = "/audio/frequency"
const val AUDIO_RAW_MIN_DEFAULT = 20f
const val AUDIO_RAW_MAX_DEFAULT = 35f
const val AUDIO_OUT_MIN_DEFAULT = 0f
const val AUDIO_OUT_MAX_DEFAULT = 1f
const val AUDIO_SMOOTHING_DEFAULT = 1

const val PORT_DEFAULT = 7348
const val IP_DEFAULT = "192.168.0.1"
const val COMBINE_OSC_DEFAULT = false
const val COMBINE_OSC_ADDRESS_DEFAULT = "/oschook"



// message IDs

const val REQUEST_FINE_LOCATION_FOR_BEACON_TO_FRAGMENT = 811
const val REQUEST_FINE_LOCATION_FOR_BEACON_TO_SETTINGS = 812
const val REQUEST_BACKGROUND_LOCATION_FOR_BEACON_TO_FRAGMENT = 821
const val REQUEST_BACKGROUND_LOCATION_FOR_BEACON_TO_SETTINGS = 822


const val REQUEST_FINE_LOCATION_FOR_EXPONOTIF_TO_FRAGMENT = 911
const val REQUEST_FINE_LOCATION_FOR_EXPONOTIF_TO_SETTINGS = 912
const val REQUEST_BACKGROUND_LOCATION_FOR_EXPONOTIF_TO_FRAGMENT = 921
const val REQUEST_BACKGROUND_LOCATION_FOR_EXPONOTIF_TO_SETTINGS = 922


const val REQUEST_FINE_LOCATION_FOR_SATNAV_TO_FRAGMENT = 711
const val REQUEST_FINE_LOCATION_FOR_SATNAV_TO_SETTINGS = 712
const val REQUEST_BACKGROUND_LOCATION_FOR_SATNAV_TO_FRAGMENT = 721
const val REQUEST_BACKGROUND_LOCATION_FOR_SATNAV_TO_SETTINGS = 722


const val REQUEST_IGNORE_BATTERY_OPTIMIZATIONS = 701
const val REQUEST_RECORD_AUDIO_TO_FRAGMENT = 601
const val REQUEST_RECORD_AUDIO_TO_SETTINGS = 602
