package com.hollyhook.oscHook.dialogs

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.CheckBox
import android.widget.SeekBar
import androidx.fragment.app.DialogFragment
import com.hollyhook.oscHook.COMBINE_OSC_DEFAULT
import com.hollyhook.oscHook.MainActivity
import com.hollyhook.oscHook.R
import kotlin.math.max
import kotlin.math.min

class AdvancedSettingsDialog : DialogFragment() {

    private val TAG = "AdvancedSettingsDialog"

    // Use this instance of the interface to get members of the parent
    lateinit var parent: MainActivity

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            parent = context as MainActivity
        } catch (e: ClassCastException) {
            Log.e(TAG, "invalid parent class")
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val factory = LayoutInflater.from(context)

        // add a custom layout to an AlertDialog
        val myView: View = factory.inflate(R.layout.dialog_settings, null)

        val speed = parent.sharedPref!!.getInt(
            getString(R.string.update_rate_key),
            1
        )
        // seekBar goes from 0..2
        (myView.findViewById(R.id.seekBar) as SeekBar).progress = speed

        val combined = parent.sharedPref!!.getBoolean(
            getString(R.string.osc_combined_key),
            COMBINE_OSC_DEFAULT
        )
        (myView.findViewById(R.id.combined_on) as CheckBox).isChecked = combined

        return AlertDialog.Builder(context)
            .setView(myView)
            .setPositiveButton(R.string.alert_dialog_ok) { _, _ ->
                // ok button clicked
                val p = (myView.findViewById<View>(R.id.seekBar) as SeekBar).progress
                val c = (myView.findViewById(R.id.combined_on) as CheckBox).isChecked

                // save values. this should trigger the listener
                val editor: SharedPreferences.Editor = parent.sharedPref!!.edit()
                editor.putInt(getString(R.string.update_rate_key), p)
                editor.putBoolean(getString(R.string.osc_combined_key), c)
                editor.apply()
            }
            .create()
    }

}