package com.hollyhook.oscHook.dialogs

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.CheckBox
import android.widget.EditText
import android.widget.TextView
import com.hollyhook.oscHook.*


class AudioDialog : PlotDialog() {
    @SuppressLint("CutPasteId")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val factory = LayoutInflater.from(context)

        // add a custom layout to an AlertDialog
        val dialog: View = factory.inflate(R.layout.dialog_audio, null)

        // restore on/off setting
        val on: Boolean = parent.sharedPref!!.getBoolean(
            getString(R.string.audio_on_key), AUDIO_ON_DEFAULT
        )
        dialog.findViewById<CheckBox>(R.id.audio_on).isChecked = on

        // OSC address for sending audio
        val splAddress = parent.sharedPref!!.getString(getString(R.string.audio_osc_spl_key),
            AUDIO_OSC_SPL_DEFAULT
        )
        dialog.findViewById<EditText>(R.id.audio_osc_spl).setText(splAddress)
        // OSC address for sending audio
        val frequencyAddress = parent.sharedPref!!.getString(getString(R.string.audio_osc_frequency_key),
            AUDIO_OSC_FREQUENCY_DEFAULT
        )
        dialog.findViewById<EditText>(R.id.audio_osc_frequency).setText(frequencyAddress)

        // input scaling range
        val minInput:Float = parent.sharedPref!!.getFloat(
                getString(R.string.audio_spl_raw_min_key),
                AUDIO_RAW_MIN_DEFAULT
        )
        dialog.findViewById<EditText>(R.id.audio_raw_min).setText(minInput.toString())
        val maxInput:Float = parent.sharedPref!!.getFloat(
            getString(R.string.audio_spl_raw_max_key),
            AUDIO_RAW_MAX_DEFAULT
        )
        dialog.findViewById<EditText>(R.id.audio_raw_max).setText(maxInput.toString())

        // output scaling range
        val minOutput:Float = parent.sharedPref!!.getFloat(
            getString(R.string.audio_spl_out_min_key),
            AUDIO_OUT_MIN_DEFAULT
        )
        dialog.findViewById<EditText>(R.id.audio_out_min).setText(minOutput.toString())
        val maxOutput:Float = parent.sharedPref!!.getFloat(
            getString(R.string.audio_spl_out_max_key),
            AUDIO_OUT_MAX_DEFAULT
        )
        dialog.findViewById<EditText>(R.id.audio_out_max).setText(maxOutput.toString())

        // slide
        val slide:Int = parent.sharedPref!!.getInt(
            getString(R.string.audio_smoothing_key),
            AUDIO_SMOOTHING_DEFAULT
        )
        dialog.findViewById<EditText>(R.id.audio_smoothing).setText(slide.toString())

        return AlertDialog.Builder(context, R.style.CustomDialogTheme)
            .setView(dialog)
            .setPositiveButton(R.string.alert_dialog_ok) { _, _ ->
                // ok button clicked
                val onNew = dialog.findViewById<CheckBox>(R.id.audio_on).isChecked
                val addressSplNew = dialog.findViewById<TextView>(R.id.audio_osc_spl).text
                val addressFrequencyNew = dialog.findViewById<TextView>(R.id.audio_osc_frequency).text
                val inputMinNew = dialog.findViewById<TextView>(R.id.audio_raw_min).text
                val inputMaxNew = dialog.findViewById<TextView>(R.id.audio_raw_max).text
                val outputMinNew = dialog.findViewById<TextView>(R.id.audio_out_min).text
                val outputMaxNew = dialog.findViewById<TextView>(R.id.audio_out_max).text
                val slideNew = dialog.findViewById<TextView>(R.id.audio_smoothing).text

                val editor: SharedPreferences.Editor = parent.sharedPref!!.edit()
                // save values
                editor.putBoolean(getString(R.string.audio_on_key), onNew)
                putIfValid(editor, R.string.audio_osc_spl_key, addressSplNew)
                putIfValid(editor, R.string.audio_osc_frequency_key, addressFrequencyNew)

                try {
                    editor.putFloat(getString(R.string.audio_spl_raw_min_key), inputMinNew.toString().toFloat())
                    editor.putFloat(getString(R.string.audio_spl_raw_max_key), inputMaxNew.toString().toFloat())
                    editor.putFloat(getString(R.string.audio_spl_out_min_key), outputMinNew.toString().toFloat())
                    editor.putFloat(getString(R.string.audio_spl_out_max_key), outputMaxNew.toString().toFloat())
                    editor.putInt(getString(R.string.audio_smoothing_key), slideNew.toString().toInt())
                } catch (ex: NumberFormatException) {}

                editor.apply()
             }
            .create()
    }
}