package com.hollyhook.oscHook.dialogs

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.CheckBox
import android.widget.EditText
import android.widget.TextView
import com.hollyhook.oscHook.*

class LinaccDialog : PlotDialog() {
    @SuppressLint("CutPasteId")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val factory = LayoutInflater.from(context)

        // add a custom layout to an AlertDialog
        val dialog: View = factory.inflate(R.layout.dialog_linacc, null)

        // restore on/off setting
        val on: Boolean = parent.sharedPref!!.getBoolean(
            getString(R.string.linacc_on_key),
            LINACC_ON_DEFAULT
        )
        dialog.findViewById<CheckBox>(R.id.linacc_on).isChecked = on

        // OSC addresses for sending linacc
        var address = parent.sharedPref!!.getString(
            getString(R.string.linacc_x_osc_key),
            LINACC_X_OSC_DEFAULT
        )
        dialog.findViewById<EditText>(R.id.linacc_osc_x).setText(address)
        address = parent.sharedPref!!.getString(
            getString(R.string.linacc_y_osc_key),
            LINACC_Y_OSC_DEFAULT
        )
        dialog.findViewById<EditText>(R.id.linacc_osc_y).setText(address)
        address = parent.sharedPref!!.getString(
            getString(R.string.linacc_z_osc_key),
            LINACC_Z_OSC_DEFAULT
        )
        dialog.findViewById<EditText>(R.id.linacc_osc_z).setText(address)

        // input scaling range
        val minInput: Float = parent.sharedPref!!.getFloat(
            getString(R.string.linacc_raw_min_key),
            LINACC_RAW_MIN_DEFAULT
        )
        dialog.findViewById<EditText>(R.id.linacc_raw_min).setText(minInput.toString())
        val maxInput: Float = parent.sharedPref!!.getFloat(
            getString(R.string.linacc_raw_max_key),
            LINACC_RAW_MAX_DEFAULT
        )
        dialog.findViewById<EditText>(R.id.linacc_raw_max).setText(maxInput.toString())

        // output scaling range
        val minOutput: Float = parent.sharedPref!!.getFloat(
            getString(R.string.linacc_out_min_key),
            LINACC_OUT_MIN_DEFAULT
        )
        dialog.findViewById<EditText>(R.id.linacc_out_min).setText(minOutput.toString())
        val maxOutput: Float = parent.sharedPref!!.getFloat(
            getString(R.string.linacc_out_max_key),
            LINACC_OUT_MAX_DEFAULT
        )
        dialog.findViewById<EditText>(R.id.linacc_out_max).setText(maxOutput.toString())

        // slide
        val slide: Int = parent.sharedPref!!.getInt(
            getString(R.string.linacc_smoothing_key),
            LINACC_SMOOTHING_DEFAULT
        )
        dialog.findViewById<EditText>(R.id.linacc_smoothing).setText(slide.toString())

        return AlertDialog.Builder(context, R.style.CustomDialogTheme)
            .setView(dialog)
            .setPositiveButton(R.string.alert_dialog_ok) { _, _ ->
                // ok button clicked
                val onNew = dialog.findViewById<CheckBox>(R.id.linacc_on).isChecked
                val addressXNew = dialog.findViewById<TextView>(R.id.linacc_osc_x).text
                val addressYNew = dialog.findViewById<TextView>(R.id.linacc_osc_y).text
                val addressZNew = dialog.findViewById<TextView>(R.id.linacc_osc_z).text
                val inputMinNew = dialog.findViewById<TextView>(R.id.linacc_raw_min).text
                val inputMaxNew = dialog.findViewById<TextView>(R.id.linacc_raw_max).text
                val outputMinNew = dialog.findViewById<TextView>(R.id.linacc_out_min).text
                val outputMaxNew = dialog.findViewById<TextView>(R.id.linacc_out_max).text
                val slideNew = dialog.findViewById<TextView>(R.id.linacc_smoothing).text

                val editor: SharedPreferences.Editor = parent.sharedPref!!.edit()

                // save values
                editor.putBoolean(getString(R.string.linacc_on_key), onNew)
                putIfValid(editor, R.string.linacc_x_osc_key, addressXNew)
                putIfValid(editor, R.string.linacc_y_osc_key, addressYNew)
                putIfValid(editor, R.string.linacc_z_osc_key, addressZNew)

                try {
                    editor.putFloat(
                        getString(R.string.linacc_raw_min_key),
                        inputMinNew.toString().toFloat()
                    )
                    editor.putFloat(
                        getString(R.string.linacc_raw_max_key),
                        inputMaxNew.toString().toFloat()
                    )
                    editor.putFloat(
                        getString(R.string.linacc_out_min_key),
                        outputMinNew.toString().toFloat()
                    )
                    editor.putFloat(
                        getString(R.string.linacc_out_max_key),
                        outputMaxNew.toString().toFloat()
                    )
                    editor.putInt(
                        getString(R.string.linacc_smoothing_key),
                        slideNew.toString().toInt()
                    )
                } catch (ex: NumberFormatException) {
                }

                editor.apply()
            }
            .create()

    }
}