package com.hollyhook.oscHook.dialogs

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import com.hollyhook.oscHook.*


class SatnavDialog : PlotDialog(),
    AdapterView.OnItemSelectedListener {

    @SuppressLint("CutPasteId")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val factory = LayoutInflater.from(context)

        // add a custom layout to an AlertDialog
        val dialog: View = factory.inflate(R.layout.dialog_satnav, null)

        // restore on/off setting
        val on: Boolean = parent.sharedPref!!.getBoolean(
            getString(R.string.satnav_on_key), SATNAV_ON_DEFAULT
        )
        dialog.findViewById<CheckBox>(R.id.satnav_on).isChecked = on

        val spinner = dialog.findViewById<Spinner>(R.id.satnav_spinner)

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter.createFromResource(
            parent,
            R.array.satnav_spinner,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinner.adapter = adapter
        }

        // restore previously selected value
        val spinnerSelection = parent.sharedPref?.getInt(
            getString(R.string.satnav_spinner_key),
                      SATNAV_SPINNER_DEFAULT)?:SATNAV_SPINNER_DEFAULT
        spinner.setSelection(spinnerSelection)
        val useOffset: Boolean = SATNAV_SPINNER_DEFAULT == spinnerSelection

        spinner.onItemSelectedListener = this

        // OSC addresses for sending longitude and latitude
        setupOscEditor(dialog,
            R.id.satnav_osc_longitude,
            SATNAV_LONGITUDE_OSC_DEFAULT,
            !useOffset,
            R.string.satnav_longitude_osc_key)
        setupOscEditor(dialog,
            R.id.satnav_osc_latitude,
            SATNAV_LATITUDE_OSC_DEFAULT,
            !useOffset,
            R.string.satnav_latitude_osc_key)

        // OSC addresses for sending distance and bearing
        setupOscEditor(dialog,
            R.id.satnav_osc_distance,
            SATNAV_DISTANCE_OSC_DEFAULT,
            useOffset,
            R.string.satnav_distance_osc_key)
        setupOscEditor(dialog,
            R.id.satnav_osc_bearing,
            SATNAV_BEARING_OSC_DEFAULT,
            useOffset,
            R.string.satnav_bearing_osc_key)

        return AlertDialog.Builder(context, R.style.CustomDialogTheme)
            .setView(dialog)
            .setPositiveButton(R.string.alert_dialog_ok) { _, _ ->
                // ok button clicked
                val onNew = dialog.findViewById<CheckBox>(R.id.satnav_on).isChecked
                val spinnerSelectionNew = dialog.findViewById<Spinner>(R.id.satnav_spinner).selectedItemPosition
                val addressXNew = dialog.findViewById<TextView>(R.id.satnav_osc_longitude).text
                val addressYNew = dialog.findViewById<TextView>(R.id.satnav_osc_latitude).text
                val addressDNew = dialog.findViewById<TextView>(R.id.satnav_osc_distance).text
                val addressBNew = dialog.findViewById<TextView>(R.id.satnav_osc_bearing).text

                val editor: SharedPreferences.Editor = parent.sharedPref!!.edit()

                // save values
                editor.putBoolean(getString(R.string.satnav_on_key), onNew)
                editor.putInt(getString(R.string.satnav_spinner_key), spinnerSelectionNew)

                putIfValid(editor, R.string.satnav_longitude_osc_key, addressXNew)
                putIfValid(editor, R.string.satnav_latitude_osc_key, addressYNew)
                putIfValid(editor, R.string.satnav_distance_osc_key, addressDNew)
                putIfValid(editor, R.string.satnav_bearing_osc_key, addressBNew)

                editor.apply()
             }
            .create()
    }

    private fun setupOscEditor(
        dialog: View,
        id: Int,
        def: String,
        enabled: Boolean,
        key: Int
    ) {
        // OSC addresses for sending longitude and latitude
        if (parent.sharedPref == null) return
        val address = parent.sharedPref!!.getString(
            getString(key), def
        )
        val e = dialog.findViewById<EditText>(id)
        e.setText(address)
        e.isEnabled = enabled
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        Log.v(TAG, "onItemSelectedListener")
        val useOffset = (position == SATNAV_SPINNER_DEFAULT)
        // update the edit fields accordingly
        for (eid:Int in setOf(R.id.satnav_osc_longitude, R.id.satnav_osc_latitude)) {
            val e = view?.rootView?.findViewById<EditText>(eid)
            e?.isEnabled = !useOffset
        }
        for (eid:Int in setOf(R.id.satnav_osc_distance, R.id.satnav_osc_bearing)) {
            val e = view?.rootView?.findViewById<EditText>(eid)
            e?.isEnabled = useOffset
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }
}