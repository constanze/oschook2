package com.hollyhook.oscHook.dialogs

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.CheckBox
import android.widget.EditText
import android.widget.TextView
import com.hollyhook.oscHook.*

class BeaconDialog : PlotDialog() {
    @SuppressLint("CutPasteId")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val factory = LayoutInflater.from(context)

        // add a custom layout to an AlertDialog
        val dialog: View = factory.inflate(R.layout.dialog_beacon, null)

        // restore on/off setting
        val on: Boolean = parent.sharedPref!!.getBoolean(
            getString(R.string.beacon_on_key), BEACON_ON_DEFAULT
        )
        dialog.findViewById<CheckBox>(R.id.beacon_on).isChecked = on

        // OSC addresses for sending beacon
        var address = parent.sharedPref!!.getString(
            getString(R.string.beacon_1_osc_key),
            BEACON_1_OSC_DEFAULT
        )
        dialog.findViewById<EditText>(R.id.beacon_osc_1).setText(address)
        address = parent.sharedPref!!.getString(
            getString(R.string.beacon_2_osc_key),
            BEACON_2_OSC_DEFAULT
        )
        dialog.findViewById<EditText>(R.id.beacon_osc_2).setText(address)
        address = parent.sharedPref!!.getString(
            getString(R.string.beacon_3_osc_key),
            BEACON_3_OSC_DEFAULT
        )
        dialog.findViewById<EditText>(R.id.beacon_osc_3).setText(address)

        address = parent.sharedPref!!.getString(
            getString(R.string.beacon_4_osc_key),
            BEACON_4_OSC_DEFAULT
        )
        dialog.findViewById<EditText>(R.id.beacon_osc_4).setText(address)

        // input scaling range
        val minInput: Float = parent.sharedPref!!.getFloat(
            getString(R.string.beacon_raw_min_key),
            BEACON_RAW_MIN_DEFAULT
        )
        dialog.findViewById<EditText>(R.id.beacon_raw_min).setText(minInput.toString())
        val maxInput: Float = parent.sharedPref!!.getFloat(
            getString(R.string.beacon_raw_max_key),
            BEACON_RAW_MAX_DEFAULT
        )
        dialog.findViewById<EditText>(R.id.beacon_raw_max).setText(maxInput.toString())

        // output scaling range
        val minOutput: Float = parent.sharedPref!!.getFloat(
            getString(R.string.beacon_out_min_key),
            BEACON_OUT_MIN_DEFAULT
        )
        dialog.findViewById<EditText>(R.id.beacon_out_min).setText(minOutput.toString())
        val maxOutput: Float = parent.sharedPref!!.getFloat(
            getString(R.string.beacon_out_max_key),
            BEACON_OUT_MAX_DEFAULT
        )
        dialog.findViewById<EditText>(R.id.beacon_out_max).setText(maxOutput.toString())

        return AlertDialog.Builder(context, R.style.CustomDialogTheme)
            .setView(dialog)
            .setPositiveButton(R.string.alert_dialog_ok) { _, _ ->
                // ok button clicked
                val onNew = dialog.findViewById<CheckBox>(R.id.beacon_on).isChecked
                val addressXNew = dialog.findViewById<TextView>(R.id.beacon_osc_1).text
                val addressYNew = dialog.findViewById<TextView>(R.id.beacon_osc_2).text
                val addressZNew = dialog.findViewById<TextView>(R.id.beacon_osc_3).text
                val addressZZNew = dialog.findViewById<TextView>(R.id.beacon_osc_4).text
                val inputMinNew = dialog.findViewById<TextView>(R.id.beacon_raw_min).text
                val inputMaxNew = dialog.findViewById<TextView>(R.id.beacon_raw_max).text
                val outputMinNew = dialog.findViewById<TextView>(R.id.beacon_out_min).text
                val outputMaxNew = dialog.findViewById<TextView>(R.id.beacon_out_max).text

                val editor: SharedPreferences.Editor = parent.sharedPref!!.edit()

                // save values
                editor.putBoolean(getString(R.string.beacon_on_key), onNew)
                putIfValid(editor, R.string.beacon_1_osc_key, addressXNew)
                putIfValid(editor, R.string.beacon_2_osc_key, addressYNew)
                putIfValid(editor, R.string.beacon_3_osc_key, addressZNew)
                putIfValid(editor, R.string.beacon_4_osc_key, addressZZNew)

                try {
                    editor.putFloat(
                        getString(R.string.beacon_raw_min_key),
                        inputMinNew.toString().toFloat()
                    )
                    editor.putFloat(
                        getString(R.string.beacon_raw_max_key),
                        inputMaxNew.toString().toFloat()
                    )
                    editor.putFloat(
                        getString(R.string.beacon_out_min_key),
                        outputMinNew.toString().toFloat()
                    )
                    editor.putFloat(
                        getString(R.string.beacon_out_max_key),
                        outputMaxNew.toString().toFloat()
                    )
                } catch (ex: NumberFormatException) {
                }

                editor.apply()
            }
            .create()

    }
}