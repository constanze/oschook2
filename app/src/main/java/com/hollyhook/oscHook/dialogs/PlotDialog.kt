package com.hollyhook.oscHook.dialogs

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.hollyhook.oscHook.MainActivity
import com.hollyhook.oscHook.R
import com.illposed.osc.OSCMessage


open class PlotDialog : DialogFragment() {
    internal val TAG = "PlotDialog"

    // Use this instance of the interface to get members of the parent
    lateinit var parent: MainActivity

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            parent = context as MainActivity
        } catch (e: ClassCastException) {
            Log.e(TAG, "invalid parent class")
        }
    }


    /**
     * check if the osc address is valid, and if ok put it to the shared pref. editor
     */
    fun putIfValid(
        editor: SharedPreferences.Editor,
        key: Int,
        address: CharSequence) {
        val adr = address.toString()
        if (adr.isEmpty() || OSCMessage.isValidAddress(adr.toString())) {
            editor.putString(getString(key), adr.toString())
        } else {
            // don't change the address, complain
            val errorStringId = if (adr.indexOf("/") == 0)
                R.string.error_osc_address // the more general error
            else
                R.string.error_osc_address_slash

            val msg = String.format(getString(errorStringId), adr)
            val toast = Toast.makeText(context, msg, Toast.LENGTH_LONG)
            toast.show()
        }
    }
}