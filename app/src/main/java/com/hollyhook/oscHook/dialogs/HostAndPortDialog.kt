package com.hollyhook.oscHook.dialogs

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.hollyhook.oscHook.IP_DEFAULT
import com.hollyhook.oscHook.MainActivity
import com.hollyhook.oscHook.PORT_DEFAULT
import com.hollyhook.oscHook.R

class HostAndPortDialog : DialogFragment() {
    private val TAG = "HostAndPortDialog"

    // Use this instance of the interface to get members of the parent
    private lateinit var parent: MainActivity

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            parent = context as MainActivity
        } catch (e: ClassCastException) {
            Log.e(TAG, "invalid parent class")
        }
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val factory = LayoutInflater.from(context)

        // add a custom layout to an AlertDialog
        val iPEntryView: View = factory.inflate(R.layout.dialog_ip_port, null)

        // restore ip
        val oldip: String? = parent.sharedPref!!.getString(
            getString(R.string.ip_key), IP_DEFAULT
        )
        (iPEntryView.findViewById(R.id.ip_edit) as EditText).setText(oldip)
        var ip = oldip

        // likewise for port
        val oldport: Int = parent.sharedPref!!.getInt(
            getString(R.string.port_key), PORT_DEFAULT
        )
        (iPEntryView.findViewById(R.id.port_edit) as EditText).setText(oldport.toString())
        var port = oldport

        return AlertDialog.Builder(context, R.style.CustomDialogTheme
        )
            .setView(iPEntryView)
            .setPositiveButton(R.string.alert_dialog_ok) { _, _ ->

                // ok button clicked
                val resIp: CharSequence =
                    iPEntryView.findViewById<TextView>(R.id.ip_edit).text
                val resPort: CharSequence =
                    iPEntryView.findViewById<TextView>(R.id.port_edit).text
                try {
                    ip = resIp.toString()
                    port = resPort.toString().toInt()
                } catch (e: java.lang.Exception) {
                    // don't change port
                    val toast = Toast.makeText(context, R.string.error_port, Toast.LENGTH_LONG)
                    toast.show()
                }
                // save values
                val editor: SharedPreferences.Editor = parent.sharedPref!!.edit()
                editor.putString(getString(R.string.ip_key), ip?.trim())
                editor.putInt(getString(R.string.port_key), port)
                editor.apply()
            }
            .create()
    }
}