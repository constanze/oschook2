package com.hollyhook.oscHook.dialogs

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.CheckBox
import android.widget.EditText
import android.widget.TextView
import com.hollyhook.oscHook.*


class OrientationDialog : PlotDialog() {

    @SuppressLint("CutPasteId")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val factory = LayoutInflater.from(context)

        // add a custom layout to an AlertDialog
        val dialog: View = factory.inflate(R.layout.dialog_orientation, null)

        // restore on/off setting
        val on: Boolean = parent.sharedPref!!.getBoolean(
            getString(R.string.orientation_on_key), ORIENTATION_ON_DEFAULT
        )
        dialog.findViewById<CheckBox>(R.id.orientation_on).isChecked = on

        // OSC addresses for sending orientation
        var address = parent.sharedPref!!.getString(getString(R.string.orientation_alpha_osc_key),
            ORIENTATION_ALPHA_OSC_DEFAULT
        )
        dialog.findViewById<EditText>(R.id.orientation_osc_alpha).setText(address)
        address = parent.sharedPref!!.getString(getString(R.string.orientation_beta_osc_key),
            ORIENTATION_BETA_OSC_DEFAULT
        )
        dialog.findViewById<EditText>(R.id.orientation_osc_beta).setText(address)
        address = parent.sharedPref!!.getString(getString(R.string.orientation_gamma_osc_key),
            ORIENTATION_GAMMA_OSC_DEFAULT
        )
        dialog.findViewById<EditText>(R.id.orientation_osc_gamma).setText(address)

        // input scaling range
        val minInput:Float = parent.sharedPref!!.getFloat(
                getString(R.string.orientation_raw_min_key),
                ORIENTATION_RAW_MIN_DEFAULT
        )
        dialog.findViewById<EditText>(R.id.orientation_raw_min).setText(minInput.toString())
        val maxInput:Float = parent.sharedPref!!.getFloat(
            getString(R.string.orientation_raw_max_key),
            ORIENTATION_RAW_MAX_DEFAULT
        )
        dialog.findViewById<EditText>(R.id.orientation_raw_max).setText(maxInput.toString())

        // output scaling range
        val minOutput:Float = parent.sharedPref!!.getFloat(
            getString(R.string.orientation_out_min_key),
            ORIENTATION_OUT_MIN_DEFAULT
        )
        dialog.findViewById<EditText>(R.id.orientation_out_min).setText(minOutput.toString())
        val maxOutput:Float = parent.sharedPref!!.getFloat(
            getString(R.string.orientation_out_max_key),
            ORIENTATION_OUT_MAX_DEFAULT
        )
        dialog.findViewById<EditText>(R.id.orientation_out_max).setText(maxOutput.toString())

        // slide
        val slide:Int = parent.sharedPref!!.getInt(
            getString(R.string.orientation_smoothing_key),
            ORIENTATION_SMOOTHING_DEFAULT
        )
        dialog.findViewById<EditText>(R.id.orientation_smoothing).setText(slide.toString())

        return AlertDialog.Builder(context, R.style.CustomDialogTheme)
            .setView(dialog)
            .setPositiveButton(R.string.alert_dialog_ok) { _, _ ->
                // ok button clicked
                val onNew = dialog.findViewById<CheckBox>(R.id.orientation_on).isChecked
                val addressXNew = dialog.findViewById<TextView>(R.id.orientation_osc_alpha).text
                val addressYNew = dialog.findViewById<TextView>(R.id.orientation_osc_beta).text
                val addressZNew = dialog.findViewById<TextView>(R.id.orientation_osc_gamma).text
                val inputMinNew = dialog.findViewById<TextView>(R.id.orientation_raw_min).text
                val inputMaxNew = dialog.findViewById<TextView>(R.id.orientation_raw_max).text
                val outputMinNew = dialog.findViewById<TextView>(R.id.orientation_out_min).text
                val outputMaxNew = dialog.findViewById<TextView>(R.id.orientation_out_max).text
                val slideNew = dialog.findViewById<TextView>(R.id.orientation_smoothing).text

                val editor: SharedPreferences.Editor = parent.sharedPref!!.edit()
                // save values
                editor.putBoolean(getString(R.string.orientation_on_key), onNew)
                putIfValid(editor, R.string.orientation_alpha_osc_key, addressXNew)
                putIfValid(editor, R.string.orientation_beta_osc_key, addressYNew)
                putIfValid(editor, R.string.orientation_gamma_osc_key, addressZNew)

                try {
                    editor.putFloat(getString(R.string.orientation_raw_min_key), inputMinNew.toString().toFloat())
                    editor.putFloat(getString(R.string.orientation_raw_max_key), inputMaxNew.toString().toFloat())
                    editor.putFloat(getString(R.string.orientation_out_min_key), outputMinNew.toString().toFloat())
                    editor.putFloat(getString(R.string.orientation_out_max_key), outputMaxNew.toString().toFloat())
                    editor.putInt(getString(R.string.orientation_smoothing_key), slideNew.toString().toInt())
                } catch (ex: NumberFormatException) {}

                editor.apply()
             }
            .create()
    }
}