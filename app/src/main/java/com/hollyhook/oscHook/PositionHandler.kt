package com.hollyhook.oscHook

import android.annotation.SuppressLint
import android.location.*
import android.location.Criteria.ACCURACY_HIGH
import android.os.Bundle
import android.util.Log

class PositionHandler(
    var locationManager: LocationManager,
    private val measurements: FloatArray,
    var useOffsets: Boolean
) : LocationListener {

    var lng = Double.NaN // last accurate value
    var lat = Double.NaN

    // first location
    var startLocation: Location? = null

    private var ready = false
    var accuracy = Float.NaN // radius of 68% confidence

    override fun onLocationChanged(location: Location) {
        if (location.hasAccuracy()) accuracy = location.accuracy
        lng = location.longitude
        lat = location.latitude

        if (useOffsets) {
            if (startLocation == null) {
                startLocation = location
                measurements[0] = 0.0f
                measurements[1] = 0.0f
                Log.v(TAG, "origin set to $location")
            } else {
                val dist = location.distanceTo(startLocation)
                measurements[0] = dist

                // value is 0 if startLocation is north of me
                // value is -90 if startLocation is west
                val b = location.bearingTo(startLocation)
                measurements[1] = b
                Log.v(TAG, "dist, bear: $dist, $b")
            }
        } else {
            // downcast to float. quoting stack overflow:
            // 32 bits gives you an E/W resolution at the equator of about 7 cm.
            // This is close to the scale that high grade GPS setups can work at
            // concern is the floating point precision in that range
            measurements[0] = lng.toFloat() // it hurts
            measurements[1] = lat.toFloat() // it hurts more
            Log.v(TAG, "lo $lng la $lat")
            // reset, in case the setting was changed
            startLocation = null
        }

        if (measurements.size > 2) {
            measurements[2] = accuracy
            Log.v(TAG, "lo $lng la $lat acc $accuracy")
        }
    }

    override fun onProviderDisabled(provider: String) {
        Log.v(TAG, "onProviderDisabled")
        ready = false
    }

    override fun onProviderEnabled(provider: String) {
        Log.v(TAG, "onProviderEnabled")
        ready = true
    }

    override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {
        Log.v(TAG, "onStatusChanged")
    }


    @SuppressLint("MissingPermission")
    fun register() {
        // permissions are checked in the activity...
        val criteria = Criteria()
        criteria.horizontalAccuracy = ACCURACY_HIGH
        val provider = locationManager.getBestProvider(criteria, true)
        Log.i(TAG, "satnav providers " + locationManager.getProviders(true))
        Log.i(TAG, "Using $provider for satnav")

        // reset starting position
        startLocation = null

        if (provider != null)
            locationManager.requestLocationUpdates(
                provider,
                1000,           // 1-second interval
                1f,         // 1 meters.
                this
            )
    }

    fun unregister() {
        locationManager.removeUpdates(this)
    }

    companion object {
        const val TAG = "oscHook.PositionHandler"
    }

    init {
        //locationManager.addNmeaListener(this)
    }
}