package com.hollyhook.oscHook

data class AgingBeacon(
    var address: String? = null,
    var timestamp: Long = 0L,
    var distance: Float = Float.NaN
)