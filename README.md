﻿# oscHook Android App

### Overview

This is an app that continuously monitors the sensors of the phone, and transmits their measurements to a host in form of OSC messages. OSC is a UDP based protocol understood by many music or visual art software, e.g. TouchDesigner, Ableton Live (with Max for Live), Resolume Arena, Reaper, etc.


![screen shot list](doc/images/device-2020-12-25-133207.png)
![screen shot accelerometer](doc/images/device-2020-12-25-133930.png)


### Getting Started

[Owen Lowery](http://www.owenlowery.com/) made an excellent video tutorial about oscHook, check out (https://vimeo.com/361651172). It is for oscHook with TouchDesigner, but the basics like IP and port setup are the same with any other host program. 


### Demos with Ableton and Max

[Demo MIDI note with Max](doc/demos/oschook_note%20Project/README.md) for Ableton Live MIDI note on, off and pitch bend, all triggered by the light sensor.

[Demo MIDI CC](doc/demos/oschook_cc%20Project/README.md) for Ableton Live, which can be mapped to any MIDI learnable parameter in Live. This uses the orientation sensor of oscHook.

### Download

At Google Play: [com.hollyhook.oschook](https://play.google.com/store/apps/details?id=com.hollyhook.oschook)

Latest apk is also checked in [here](https://bitbucket.org/constanze/oschook2/src/master/app/release/app-release.apk)


## Documentation

### Sensors

1. The __light__ sensors measures the Ambient light level in SI lux units.
2. The __accelerometer__ gives the (negative) direction of the gravity as long as the device is not moved. It very fast indicates change the moment the device is moved. For instance when the device lies flat on a table and is pushed on its left side toward the right, the x acceleration value is positive.
3. __Linear acceleration__ is same than the accelerometer, but the effect of the gravity is taken out. If the device is at rest, all values are zero. 
4. The __orientation__ sensor gives 3 values: α, β, γ, following the definitions in this [w3c document](https://w3c.github.io/deviceorientation/spec-source-orientation.html).
 It gives orientation of the device relative to a world coordinate system. For example, if the device lies flat on a table with the top of the screen pointing West, this is the orientation measurement: alpha = 90°, beta = 0°, and gamma = 0°. Rotating the device like a compass will change alpha. 
 A device must have a gyroscope and a calibrated compass for optimum results. The unscaled value ranges are α in [0, 360), β in [-180, 180) and γ in [-90, 90)    
5. You can get information about the location of the device with the help of the __beacon distances__. Beacons are cheap devices transmitting on Bluetooth LE, for instance there is Apples [iBeacon](https://en.wikipedia.org/wiki/IBeacon), but beacons of other vendors are also detected. It is also possible to simulate beacons with a second Android phone with the app [Beacon Simulator](https://play.google.com/store/apps/details?id=net.alea.beaconsimulator).
 The Eddystone template works well.
6. __Exposure Notifications__ were introduced by Apple and Google to facilitate digital contact tracing during the COVID-19 pandemic. Like the beacons, they are transmitted via Bluetooth LE, thus allowing to measure the distance to the mobile  sending Exposure Notifications. In the settings dialog, the tracking time is configurable. It is the time how long the ID of the phone is kept after last received. Latest 20 min after a phone ID wasn't seen anymore, the ID is dropped. Note: In order to preserve anonymity of the transmitting phones, the ID is anyway changed every 15-20 minutes.  
7. __GPS__ sends the location derived from any [global navigation satellite system](https://en.wikipedia.org/wiki/Satellite_navigation) the mobile supports (can be GPS, but also Galileo, Beidou, ...). It is possible to signal longitude/latitude data as OSC. Since this data are somewhat difficult to interpret, oscHook alternatively reports the position relative to an origin. The origin is the first coordinate received. Since there is sometimes a drift in the position, you can re-set the origin to the current position.   
8. To enable an __audio__-reactive installation, oscHook can use the microphone,
 and transmit the Sound Pressure Level [SPL](https://en.wikipedia.org/wiki/Sound_pressure#Sound_pressure_level), and the frequency bin, where the maximum SPL was detected.


### Sensor Specific Settings

![screen shot sensor settings](doc/images/settings.png)

Each sensor has its own settings dialog, as shown in this picture for the device orientation. If you want oscHook to actually send OSC messages, switch it on by tapping on the __Send OSC__ checkbox in the top right corner. Switch off all sensors you don't need, esp. if you are going to use the high update rate. 

__Smoothing steps__ is for enabling simple a logarithmic filter between input changes. The higher the number of steps, the softer is the reaction to a change in the data. Disable the filter by setting the value to 0 or 1, if you want to react quickly on data changes.

The next four inputs are for scaling of the __data range__ of the measurements to an __output range__. It is handy when the application running on the host expects the data in a certain value. For instance in Resolume Arena, most of the values are expected in range [0, 1). If the input data range is [-180, 180), you can scale it to the range 0..1 with the output range values shown in the picture above.

Scaling is disabled when the lower and upper value of __data range__ is identical to __output range__ (the actual values don't matter in this case).

Finally, you can edit the __OSC addresses__ to whatever the host needs. If you remove an particular address altogether, OSC will not be sent for that measurement.

### Advanced Settings

![screen shot advanced settings](doc/images/advanced.png)

The advanced settings are in the main menu. Tap on the 3 dots "hamburger" while you see the list. First it is possible to set how often OSC is send. This setting is only relevant for sensors which continuously send data. If __slow__ is selected, OSC will be send approximately every 200 ms, or 5 times per second. __high__ is approximately every 20ms. With this setting, it is easy to flood the host with too much data, therefore it is recommended to switch off all sensors you don't need. To do so, go to the sensor specific settings and uncheck the __Send OSC__ boxes. The value in the middle is for an update rate of approx. 60ms. 

This setting does not affect the timing of beacons, exposure notifications, and one-shot sensors.

__Combined OSC__ is to make synchronized processing on the host easier. It is mostly needed for programmatically processing the data. Instead of sending several OSC messages, oscHook will send only one message, containing all measurements which are enabled. The order of the data is the same as the in the menu, i.e. the light measurement is the first data element, accelerometer/x the next, etc. The last data element in the OSC message is a timestamp in milliseconds.

Since enabling __Combined OSC__ reduces the amount of data sent around, it is recommended to use it in combination with a high OSC sending frequency, if many sensors are enabled.

### Background Mode

![screen shot backgound button and notification button](doc/images/background.png)

In background mode, the app permanently measures and sends OSC while the screen is off. In this mode, oscHook disables Android battery optimization measures, in order to stay awake. That implies that you have to explicitly quit the app while it is in background mode. To do so, go to the notification, and pull it down. There you find the __quit__ button, exiting the app from background mode.


## Integrations & Licenses

 - GraphView, see https://github.com/jjoe64/GraphView/blob/master/license.txt
 - JavaOSC, see https://github.com/hoijui/JavaOSC/blob/master/LICENSE.md
 - SLF4J, see http://www.slf4j.org/license.html
 - Android Beacon Library, see https://github.com/AltBeacon/android-beacon-library/blob/master/LICENSE
 
 oscHook is licensed under the Apache License 2.0

