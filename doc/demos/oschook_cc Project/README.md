# Demo MIDI CC in Ableton LIVE

To demonstrate oscHook, this is a Live set containing a Max for Live device. It receives orientation data sent by oscHook, and converts it to MIDI cc output. This can be mapped to any MIDI-learnable parameter in Live. In the demo, it is mapped to the distort parameter of the 2nd track.

![Ableton screen shot](oschook_cc.gif)

[Download the Live set](https://bitbucket.org/constanze/oschook2/raw/master/doc/demos/oschook_cc%20Project.zip)

## Installation

 - find out the IP address of your host where Ableton live runs, e.g. 192.168.43.124
 - take care that the host and the phone are in the same Wifi, and no firewall blocks communication from phone to host
 - start oscHook app on the phone
 - select menu, _IP and port_, enter the IP address, remember the port number, e.g. 7348
 - You need to set up a virtual MIDI bus. Instructions are [here](https://help.ableton.com/hc/en-us/articles/209774225-Using-virtual-MIDI-buses)
 - Run Ableton Live, and open the Live Set
 - in the M4L device, click into the OSC port textarea, enter the same as in the app, e.g. 7348 and hit the return key.
 - In the oscHook track, configure _MIDI To_ using the virtual MIDI bus
 - Likewise, the Chord track, configure _MIDI From_ using the virtual MIDI bus

On Windows, with using loopMIDI it looks like this

![another Ableton screen shot](loopmidi.png)

## Running the App

On the oscHoo app, open the Orientation settings dialog, and enable sending OSC. As soon as incoming OSC data from the app is received, you see a little blinking dot near the OSC port. If you lay the phone flat on a table, and start turning it like a compass, the values change between 0 and 360°. The patch is simple, I hope you can use it patch as a starting point for your own ideas!



--------------------

oscHook is licensed under the Apache License 2.0. Source code at https://bitbucket.org/constanze/oschook/





