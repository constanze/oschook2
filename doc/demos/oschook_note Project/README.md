# Demo Note on/off and pitch bend in Ableton LIVE

To demonstrate oscHook, here is a set containing a little Max for Live device.

![Ableton screen shot](oschook_note.gif)

[Download the Live set](https://bitbucket.org/constanze/oschook2/raw/master/doc/demos/oschook_note%20Project.zip)

The Max for Live device receives OSC values from oscHook for the light. It creates a MIDI Note On, 
as soon as the light is higher than a threshold. And Note Off when the light intensity goes below that.
At the same time it sends MIDI pitch bend, the higher the brighter the light is.

This is how it works:

 - requires Live 10.1.30+ and Max for Live (the amxd file also works with older versions)
 - find out the IP address of your host where Ableton Live runs, e.g. 192.168.0.1
 - take care that the host and the phone are in the same Wifi, and no firewall blocks communication from phone to host
 - start oscHook app on the phone
 - in the app, select menu, _IP and port_, enter the IP address, remember the port number, e.g. 7348
 - run Ableton Live, and open the Live Set
 - in the M4L device, click into the port text area, and enter the port number, e.g. 7348, and hit the return key.

You should now see incoming data, indicated by a little blinking dot near the port.

Cover your phone with the hand, and see / hear the effect!

If the values are a little off, open the app, tap on the light settings dialog and adjust the scaling.

![oscHook light setup screen shot](light_setup.png)

The upper limit of the input data range depends on the current brightness. If you 
are outside with sun, it can be as high as 40000, if you are inside it is 1000, or even less.

Change the value, so that the output is roughly between 0 and 1 (this is what the max patch is designed for).

If you wish to measure your current light in lux, you can disable scaling altogether. To do so, 
go again into the light settings dialog of oscHook, and set the values of the 
output range identically to the input data range. Then watch the values in the oscHook plot.
The highest value you see in the plot gives you a good hint what to take as a upper data range.

--------------------

oscHook is licensed under the Apache License 2.0. Source code and demos at https://bitbucket.org/constanze/oschook2