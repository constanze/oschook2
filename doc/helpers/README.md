### oscmap.maxpat

![screen shot](../images/oscmap.png)

Scale and map OSC messages. Useful if you want to connect the app with Resolume Arena.

You need at least the Max trial version to run it. (https://cycling74.com/downloads)
